   /* 
    * Beaglebone Pru IO 
    * 
    * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
    * 
    * This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    * 
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    * 
    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <beaglebone_pruio.h>
#include <beaglebone_pruio_pins.h>

/////////////////////////////////////////////////////////////////////
unsigned int finished = 0;
void signal_handler(int signal){
   finished = 1;
}

/////////////////////////////////////////////////////////////////////
static pthread_t monitor_thread;

static void* monitor_inputs(void* param){
   beaglebone_pruio_message message;
   beaglebone_pruio_midi_message midi_message;
   while(!finished){
      while(beaglebone_pruio_midi_messages_are_available() && !finished){
         /* printf("Midi\n"); */
         beaglebone_pruio_read_midi_message(&midi_message);
         printf("MIDI message: 0x%X 0x%X 0x%X %i\n", midi_message.byte0, midi_message.byte1, midi_message.byte2, midi_message.is_interesting);
      }

      while(beaglebone_pruio_messages_are_available() && !finished){
         /* printf("pruio\n"); */
         beaglebone_pruio_read_message(&message);

         if(message.is_gpio){
            printf("GPIO number:%u, Value: %f\n", message.gpio_number, message.value);
         }
         else { // message is adc 
            /* printf("ADC Channel :%u, Value: %f, Raw Value: %i\n", message.adc_channel, message.value, message.raw_value); */ 
            /* if(message.adc_channel==5 || message.adc_channel==3) { */
               printf("C:%2u, V:%0.4f\n", message.adc_channel, message.value); 
            /* } */
         } 
      } 
      usleep(100000); 
   }

   return NULL;
}

static int start_monitor_thread(){
   // TODO: set real time priority to this thread
   pthread_attr_t attr;
   if(pthread_attr_init(&attr)){
      return 1;
   }
   if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)){
      return 1;
   }
   if(pthread_create(&monitor_thread, &attr, &monitor_inputs, NULL)){
      return 1;
   }

   return 0;
}

static void stop_monitor_thread(){
   pthread_cancel(monitor_thread);
}

int main(int argc, const char *argv[]){
   srand(time(NULL));

   // Listen to SIGINT signals (program termination)
   signal(SIGINT, signal_handler);

   beaglebone_pruio_start();

   start_monitor_thread();

   // Initialize p9_12 (led sub) as output
   if(beaglebone_pruio_init_gpio_pin(P9_12, BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT)){
      fprintf(stderr, "%s\n", "Could not initialize pin P9_12");
      return 1;
   }

   // Initialize p9_16 (key 2) as input
   // if(beaglebone_pruio_init_gpio_pin(P9_16, BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)){
   //    fprintf(stderr, "%s\n", "Could not initialize pin P9_16");
   //    return 1;
   // }
   
   // Init p9_41 as input (Key 1)
   if(beaglebone_pruio_init_gpio_pin(P9_41A, BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)){
      fprintf(stderr, "%s\n", "Could not initialize pin P9_41A");
      return 1;
   }
   if(beaglebone_pruio_init_gpio_pin(P9_41B, BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)){
      fprintf(stderr, "%s\n", "Could not initialize pin P9_41B");
      return 1;
   }

   if(beaglebone_pruio_init_gpio_pin(P9_16, BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)){
      fprintf(stderr, "%s\n", "Could not initialize pin P9_16");
      return 1;
   }

   #define NUM_CHANNELS 11
   int channels[NUM_CHANNELS] = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
   for(size_t i=0; i<NUM_CHANNELS; ++i) {
      if(beaglebone_pruio_init_adc_pin(channels[i], 8, 128)){  // Channel i, 8 bits, average 128
         fprintf(stderr, "Could not initialize adc channel %i\n", channels[i]);
         return 1;
      }
   }
   
   beaglebone_pruio_set_midi_channel(3);

   int led = 0;
   while(!finished){
      led = !led;
      beaglebone_pruio_set_pin_value(P9_12, led);

      unsigned char byte0 = ( 0x9 << 4 ) | 0x0;  // Note on on channel 1
      beaglebone_pruio_write_midi_message(byte0);
      unsigned char byte1 = rand()%127;          // Random note on
      beaglebone_pruio_write_midi_message(byte1);
      unsigned char byte2 = 0x40;                  // velocity 65
      beaglebone_pruio_write_midi_message(byte2);

      usleep(0.3 * 1000000);
   }

   beaglebone_pruio_stop();
   stop_monitor_thread();

   return 0;
}
