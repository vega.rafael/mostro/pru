/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BEAGLEBONE_PRUIO_H
#define BEAGLEBONE_PRUIO_H

#include <string.h>

/**
 * A structure for easy reading of incoming messages.
 */
typedef struct beaglebone_pruio_message{
   int is_gpio; // 1 if gpio, 0 if adc
   float value;
   int adc_channel;
   int gpio_number;
} beaglebone_pruio_message;

typedef struct beaglebone_pruio_midi_message{
   char byte2;
   char byte1;
   char byte0;
   char is_interesting;
} beaglebone_pruio_midi_message;

typedef enum{  
   BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT = 0,
   BEAGLEBONE_PRUIO_GPIO_MODE_INPUT = 1
} beaglebone_pruio_gpio_mode;

typedef enum{  
   BEAGLEBONE_PRUIO_ADC_MODE_OFF = 0,
   BEAGLEBONE_PRUIO_ADC_MODE_NORMAL = 1,
} beaglebone_pruio_adc_mode;

/**
 * Initializes PRU, GPIO and ADC hardware and starts sampling ADC channels.
 */
int beaglebone_pruio_start();

/**
 * Stops PRU and ADC hardware, no more samples are aquired.
 */
int beaglebone_pruio_stop();

/**
 * Configures how a GPIO channel is used
 */
int beaglebone_pruio_init_gpio_pin(int gpio_number, beaglebone_pruio_gpio_mode mode);  

/**
 * Sets the value of an output pin (0 or 1)
 */
void beaglebone_pruio_set_pin_value(int gpio_number, int value);

/**
 * Starts reading from an ADC pin.
 */
int beaglebone_pruio_init_adc_pin(int channel_number, int bits, int average_window_size); 

/**
 * Change the midi channel we're listening to.
 */
int beaglebone_pruio_set_midi_channel(int channel);

/**
 * Returns 1 if there is data available from the PRU0
 */
static inline int beaglebone_pruio_messages_are_available();

/**
 * Puts the next message available from the PRU0 in the message address.
 */
static inline void beaglebone_pruio_read_message(beaglebone_pruio_message *message);

/**
 * Returns 1 if there is data available from the PRU1
 */
static inline int beaglebone_pruio_midi_messages_are_available();

/**
 * Puts the next message available from the PRU1 in the message address.
 */
static inline void beaglebone_pruio_read_midi_message(beaglebone_pruio_midi_message *message);

/**
 * Sends a midi message to the PRU1.
 */
static inline void beaglebone_pruio_write_midi_message(unsigned char msg);

///////////////////////////////////////////////////////////////////////////////
// !!!
// Not safe to use anything below this line from client code.
// We've put it here just so the compiler can inline it.
///////////////////////////////////////////////////////////////////////////////


// READ ALL THE STUFF REGARDING COMMUNICATION BETWEEN PRU AND ARM IN definitions.h

volatile unsigned int *beaglebone_pruio_shared_ram;

volatile unsigned int *beaglebone_pruio_buffer_0_data;
unsigned int beaglebone_pruio_buffer_0_size;
volatile unsigned int *beaglebone_pruio_buffer_0_start;
volatile unsigned int *beaglebone_pruio_buffer_0_end;

static inline __attribute__ ((always_inline)) int beaglebone_pruio_messages_are_available(){
   return (*beaglebone_pruio_buffer_0_start != *beaglebone_pruio_buffer_0_end);
}

static inline __attribute__ ((always_inline)) void beaglebone_pruio_read_message(beaglebone_pruio_message* message){
   unsigned int raw_message = beaglebone_pruio_buffer_0_data[*beaglebone_pruio_buffer_0_start & (beaglebone_pruio_buffer_0_size-1)];

   message->is_gpio = (raw_message&(1<<31))==0;
   if(message->is_gpio){
      message->value = (raw_message&(1<<8))==0;
      message->gpio_number = raw_message & 0xFF;
   }
   else{
      /* message->raw_value = (raw_message >> 4) & 0xFF; */
      /* message->value = ( (float)message->raw_value / 255.0 ); */

      unsigned int raw_value = (raw_message >> 4) & 0xFF; 
      message->value = ( (float)raw_value / 255.0 );
      message->adc_channel = raw_message & 0xF;
   }

   // Don't write buffer start before reading message (mem barrier)
   // http://stackoverflow.com/questions/982129/what-does-sync-synchronize-do
   // https://en.wikipedia.org/wiki/Memory_ordering#Compiler_memory_barrier
   __sync_synchronize();

   // Increment buffer start, wrap around 2*size
   *beaglebone_pruio_buffer_0_start = (*beaglebone_pruio_buffer_0_start+1) & (2*beaglebone_pruio_buffer_0_size - 1);
}

volatile unsigned int *beaglebone_pruio_buffer_1_data;
unsigned int beaglebone_pruio_buffer_1_size;
volatile unsigned int *beaglebone_pruio_buffer_1_start;
volatile unsigned int *beaglebone_pruio_buffer_1_end;

static inline __attribute__ ((always_inline)) int beaglebone_pruio_midi_messages_are_available(){
   return (*beaglebone_pruio_buffer_1_start != *beaglebone_pruio_buffer_1_end);
}

static inline __attribute__ ((always_inline)) void beaglebone_pruio_read_midi_message(beaglebone_pruio_midi_message* message){

   unsigned int raw_message = beaglebone_pruio_buffer_1_data[*beaglebone_pruio_buffer_1_start & (beaglebone_pruio_buffer_1_size-1)];
   message->byte0 = (raw_message>>16) & 0xFF;
   message->byte1 = (raw_message>>8) & 0xFF;
   message->byte2 = raw_message & 0xFF;
   message->is_interesting = (raw_message>>24) & 0x1;

   // Don't write buffer start before reading message (mem barrier)
   // http://stackoverflow.com/questions/982129/what-does-sync-synchronize-do
   // https://en.wikipedia.org/wiki/Memory_ordering#Compiler_memory_barrier
   __sync_synchronize();

   // Increment buffer start, wrap around 2*size
   *beaglebone_pruio_buffer_1_start = (*beaglebone_pruio_buffer_1_start+1) & (2*beaglebone_pruio_buffer_1_size - 1);
}

unsigned int beaglebone_pruio_buffer_2_size;
volatile unsigned char *beaglebone_pruio_buffer_2_data;
volatile unsigned int *beaglebone_pruio_buffer_2_start;
volatile unsigned int *beaglebone_pruio_buffer_2_end;

static inline __attribute__ ((always_inline)) void 
   beaglebone_pruio_write_midi_message(unsigned char msg){
      unsigned int is_full = 
         (*beaglebone_pruio_buffer_2_end == (*beaglebone_pruio_buffer_2_start ^ beaglebone_pruio_buffer_2_size)); // ^ is orex 
      if(!is_full){
         __sync_synchronize();
         beaglebone_pruio_buffer_2_data[*beaglebone_pruio_buffer_2_end & (beaglebone_pruio_buffer_2_size-1)] = msg;
         // Increment buffer end, wrap around 2*size
         *beaglebone_pruio_buffer_2_end = (*beaglebone_pruio_buffer_2_end+1) & (2*beaglebone_pruio_buffer_2_size - 1);
      }
}

#endif // BEAGLEBONE_PRUIO_H
