/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <math.h>

#include "beaglebone_pruio.h"
#include "definitions.h"
#include "beaglebone_pruio_pins.h"

#ifndef BEAGLEBONE_PRUIO_START_ADDR_0
   #error "BEAGLEBONE_PRUIO_START_ADDR_0 must be defined."
#endif

#ifndef BEAGLEBONE_PRUIO_PREFIX
   #error "BEAGLEBONE_PRUIO_PREFIX must be defined."
#endif

#ifndef BEAGLEBONE_GPIO_PATH
   #error "BEAGLEBONE_GPIO_PATH must be defined."
#endif

#ifndef BEAGLEBONE_CAPEMGR_PATH
   #error "BEAGLEBONE_CAPEMGR_PATH must be defined."
#endif

/* #define DEBUG */


/////////////////////////////////////////////////////////////////////
// MEMORY MAP

volatile unsigned int* gpio0_output_enable = NULL;
volatile unsigned int* gpio1_output_enable = NULL;
volatile unsigned int* gpio2_output_enable = NULL;
volatile unsigned int* gpio3_output_enable = NULL;

volatile unsigned int* gpio0_data_out = NULL;
volatile unsigned int* gpio1_data_out = NULL;
volatile unsigned int* gpio2_data_out = NULL;
volatile unsigned int* gpio3_data_out = NULL;

static int map_device_registers(){
   // Get pointers to hardware registers. See memory map in manual for addresses.
   int memdev = open("/dev/mem", O_RDWR | O_SYNC);
   
   // Get pointer to gpio0 registers (start at address 0x44e07000, length 0x1000 (4KB)).
   volatile void* gpio0 = mmap(0, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, memdev, GPIO0);
   if(gpio0 == MAP_FAILED){ return 1; }
   gpio0_output_enable = (volatile unsigned int*)(gpio0 + GPIO_OE);
   gpio0_data_out = (volatile unsigned int*)(gpio0 + GPIO_DATAOUT);

   // same for gpio1, 2 and 3.
   volatile void* gpio1 = mmap(0, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, memdev, GPIO1);
   if(gpio1 == MAP_FAILED){
      return 1;
   }
   gpio1_output_enable = (volatile unsigned int*)(gpio1 + GPIO_OE);
   gpio1_data_out = (volatile unsigned int*)(gpio1 + GPIO_DATAOUT);

   volatile void* gpio2 = mmap(0, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, memdev, GPIO2);
   if(gpio2 == MAP_FAILED){
      return 1;
   }
   gpio2_output_enable = (volatile unsigned int*)(gpio2 + GPIO_OE);
   gpio2_data_out = (volatile unsigned int*)(gpio2 + GPIO_DATAOUT);

   volatile void* gpio3 = mmap(0, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, memdev, GPIO3);
   if(gpio3 == MAP_FAILED){
      return 1;
   }
   gpio3_output_enable = (volatile unsigned int*)(gpio3 + GPIO_OE);
   gpio3_data_out = (volatile unsigned int*)(gpio3 + GPIO_DATAOUT);

   return 0;
}

/////////////////////////////////////////////////////////////////////
// GPIO PINS

typedef struct gpio_pin{ 
   beaglebone_pruio_gpio_mode mode;
   int gpio_number;
} gpio_pin;
static gpio_pin used_pins[BEAGLEBONE_PRUIO_MAX_GPIO_CHANNELS];
static int used_pins_count = 0;

static int init_gpio(){
   // Only pinmux is set here, enabling GPIO modules, 
   // clocks, debounce, etc. is set on the PRU side.

   // Pins used to control analog mux:
   int e1 = beaglebone_pruio_init_gpio_pin(P8_27, BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT);
   int e2 = beaglebone_pruio_init_gpio_pin(P8_28, BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT);
   int e3 = beaglebone_pruio_init_gpio_pin(P8_29, BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT); 

   if(e1 || e2 || e3){
      return 1;
   } 
   else{
      return 0;
   }
}

/////////////////////////////////////////////////////////////////////
// ADC CHANNELS

typedef struct adc_channel{ 
   unsigned char channel_number;
   beaglebone_pruio_adc_mode mode;
   unsigned char parameter1;
   unsigned char parameter2;
   unsigned char parameter3;
} adc_channel;

static adc_channel used_adc_channels[BEAGLEBONE_PRUIO_MAX_ADC_CHANNELS];
static int used_adc_channels_count = 0;

static int init_adc_channel(unsigned char channel_number, beaglebone_pruio_adc_mode mode, unsigned char parameter1, unsigned char parameter2, unsigned char parameter3){
   // Check if channel already in use.
   int i;
   for(i=0; i<used_adc_channels_count; ++i){
      adc_channel channel = used_adc_channels[i];
      if(channel.channel_number==channel_number){
         if(channel.mode==mode && channel.parameter1==parameter1 && channel.parameter2==parameter2 && channel.parameter3==parameter3){
            return 0;
         }
         else{
            return 1;
         }
      }
   }

   // Save new channel info;
   adc_channel new_channel;
   new_channel.channel_number = channel_number;
   new_channel.mode = mode;
   new_channel.parameter1 = parameter1;
   new_channel.parameter2 = parameter2;
   new_channel.parameter3 = parameter3;
   used_adc_channels[used_adc_channels_count] = new_channel;
   used_adc_channels_count++;

   /** 
    * Tell the PRU that we are interested in input from this channel.
    * See comments in definitions.h
    */
   unsigned int config = (mode << 28) | (parameter3 << 16) | (parameter2 << 8) | (parameter1);
   beaglebone_pruio_shared_ram[ADC0_CONFIG+channel_number] = config;

   return 0;
}

/////////////////////////////////////////////////////////////////////
// PRU Initialization
//

// http://processors.wiki.ti.com/index.php/PRU_Linux_Application_Loader_API_Guide

static int init_pru_system(){
   tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
   if(prussdrv_init()) return 1;
   if(prussdrv_open(PRU_EVTOUT_0)) return 1;
   if(prussdrv_pruintc_init(&pruss_intc_initdata)) return 1;

   // Get pointer to shared ram
   void* p;
   if(prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, &p)) return 1;
   beaglebone_pruio_shared_ram = (volatile unsigned int*)p;

   return 0;
}

static int start_pru0_program(){
   char path[512] = "";
   strcat(path, BEAGLEBONE_PRUIO_PREFIX); 
   strcat(path, "/lib/libbeaglebone_pruio_data0.bin");
   if(prussdrv_load_datafile(0, path)) return 1;

   strcpy(path, BEAGLEBONE_PRUIO_PREFIX); 
   strcat(path, "/lib/libbeaglebone_pruio_text0.bin");
   if(prussdrv_exec_program_at(0, path, BEAGLEBONE_PRUIO_START_ADDR_0)) return 1;

   return 0;
}

static int start_pru1_program(){
   char path[512] = "";
   strcat(path, BEAGLEBONE_PRUIO_PREFIX); 
   strcat(path, "/lib/libbeaglebone_pruio_data1.bin");
   if(prussdrv_load_datafile(1, path)) return 1;

   strcpy(path, BEAGLEBONE_PRUIO_PREFIX); 
   strcat(path, "/lib/libbeaglebone_pruio_text1.bin");
   if(prussdrv_exec_program_at(1, path, BEAGLEBONE_PRUIO_START_ADDR_1)) return 1;

   return 0;
}

/////////////////////////////////////////////////////////////////////////
// Ring buffer (see header file for more)
//

static void buffer_init(){
   // These shared ram positions control which adc and gpio channels
   // we want to receive data from, see comments in definitions.h
   beaglebone_pruio_shared_ram[GPIO0_CONFIG] = 0;
   beaglebone_pruio_shared_ram[GPIO1_CONFIG] = 0;
   beaglebone_pruio_shared_ram[GPIO2_CONFIG] = 0;
   beaglebone_pruio_shared_ram[GPIO3_CONFIG] = 0;

   // These positions hold the options/parameters for each of the 14
   // adc channels
   beaglebone_pruio_shared_ram[ADC0_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC1_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC2_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC3_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC4_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC5_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC6_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC7_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC8_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC9_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC10_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC11_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC12_CONFIG] = 0;
   beaglebone_pruio_shared_ram[ADC13_CONFIG] = 0;

   beaglebone_pruio_buffer_0_data = &(beaglebone_pruio_shared_ram[RING_BUFFER_0_ADDRESS]);
   beaglebone_pruio_buffer_0_size = RING_BUFFER_0_SIZE;
   beaglebone_pruio_buffer_0_start = &(beaglebone_pruio_shared_ram[RING_BUFFER_0_START]); // value inited to 0 in pru
   beaglebone_pruio_buffer_0_end = &(beaglebone_pruio_shared_ram[RING_BUFFER_0_END]); // value inited to 0 in pru

   // This position holds parameters for the midi i/o
   beaglebone_pruio_shared_ram[MIDI_CONFIG] = 0;

   beaglebone_pruio_buffer_1_data = &(beaglebone_pruio_shared_ram[RING_BUFFER_1_ADDRESS]);
   beaglebone_pruio_buffer_1_size = RING_BUFFER_1_SIZE;
   beaglebone_pruio_buffer_1_start = &(beaglebone_pruio_shared_ram[RING_BUFFER_1_START]); 
   beaglebone_pruio_buffer_1_end = &(beaglebone_pruio_shared_ram[RING_BUFFER_1_END]); 

   beaglebone_pruio_buffer_2_data = (unsigned char *) &(beaglebone_pruio_shared_ram[RING_BUFFER_2_ADDRESS]);
   beaglebone_pruio_buffer_2_size = RING_BUFFER_2_SIZE_BYTES;
   beaglebone_pruio_buffer_2_start = &(beaglebone_pruio_shared_ram[RING_BUFFER_2_START]); 
   beaglebone_pruio_buffer_2_end = &(beaglebone_pruio_shared_ram[RING_BUFFER_2_END]); 
}

/////////////////////////////////////////////////////////////////////
// "Public" functions.
//

int beaglebone_pruio_start(){

   printf("%u\n", ADC_DATA_ADDRESS + ADC_DATA_SIZE);

   // Device tree is now static (no overlay) and loaded at boot time
   #if 0  
   if(load_device_tree_overlays()){
      fprintf(stderr, "libbeaglebone_pruio: Could not load device tree overlays.\n");
      return 1;
   }
   #endif 

   if(map_device_registers()){
      fprintf(stderr, "libbeaglebone_pruio: Could not map device's registers to memory.\n");
      return 1;
   }

   if(init_pru_system()){
      fprintf(stderr, "libbeaglebone_pruio: Could not init PRU system.\n");
      return 1;
   }

   buffer_init();

   if(start_pru0_program()){
      fprintf(stderr, "libbeaglebone_pruio: Could not load PRU0 program.\n");
      return 1;
   }

   if(start_pru1_program()){
      fprintf(stderr, "libbeaglebone_pruio: Could not load PRU1 program.\n");
      return 1;
   }

   if(init_gpio()){
      fprintf(stderr, "libbeaglebone_pruio: Could not init GPIO.\n");
      return 1;
   }

   return 0;
}

int beaglebone_pruio_init_adc_pin(int channel_number, int bits, int average_window_size){
  // Check if channel number in range   
  if( channel_number<0 || channel_number>13 ) {
    fprintf(stderr, "libbeaglebone_pruio: ADC channel number must be in [0, 13].\n");
    return 1;
  }

  // Check if average_window_size is a power of 2  
  if( ((average_window_size & (average_window_size-1)) != 0) || average_window_size==0) {
    fprintf(stderr, "libbeaglebone_pruio: Average size must be a power of 2.\n");
    return 1;
  }

  // Check average_window_size in [1, 256]
  if( ( average_window_size<1) || (average_window_size>256) ) {
    fprintf(stderr, "libbeaglebone_pruio: average_window_size must be [1, 256]\n");
    return 1;
  }

  // Check number of bits between 1 and 8
  if( (bits<1) || (bits>8) ) {
    fprintf(stderr, "libbeaglebone_pruio: Number of bits must be within [1, 8]\n");
    return 1;
  }

  unsigned int shift_places = log2(average_window_size);
  return init_adc_channel((unsigned char)channel_number, BEAGLEBONE_PRUIO_ADC_MODE_NORMAL, bits, shift_places, 0);
}

int beaglebone_pruio_init_gpio_pin(int gpio_number, beaglebone_pruio_gpio_mode mode){
   // Check if pin already in use.
   int i;
   for(i=0; i<used_pins_count; ++i){
      gpio_pin pin = used_pins[i];
      if(pin.gpio_number==gpio_number && pin.mode==mode){
         return 0;
      }
      else if(pin.gpio_number==gpio_number && pin.mode!=mode){
         return 1;
      }
   }

   // Save new pin info;
   gpio_pin new_pin;
   new_pin.gpio_number = gpio_number;
   new_pin.mode = mode;
   used_pins[used_pins_count] = new_pin;
   used_pins_count++;


   // Commenting this out since device tree is now static (no pinmux config available at runtime)
   // Set the pinmux of the pin by writing to the appropriate config file
   // char path[256] = "";
   // if(get_gpio_config_file(gpio_number, path)){
   //    return 1;
   // }
   // FILE *f = fopen(path, "w");
   // if(f==NULL){
   //    return 1;
   // }
   int gpio_module = gpio_number >> 5;
   int gpio_bit = gpio_number % 32;
   if(mode == BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT){
      // fprintf(f, "%s", "output"); 
      // Clear the output enable bit in the gpio config register to actually enable output.
      switch(gpio_module){
         case 0: *gpio0_output_enable &= ~(1<<gpio_bit); break;
         case 1: *gpio1_output_enable &= ~(1<<gpio_bit); break;
         case 2: *gpio2_output_enable &= ~(1<<gpio_bit); break;
         case 3: *gpio3_output_enable &= ~(1<<gpio_bit); break;
      }
   }
   else{
      // fprintf(f, "%s", "input"); 
      // Set the output enable bit in the gpio config register to disable output.
      switch(gpio_module){
         case 0: *gpio0_output_enable |= (1<<gpio_bit); break;
         case 1: *gpio1_output_enable |= (1<<gpio_bit); break;
         case 2: *gpio2_output_enable |= (1<<gpio_bit); break;
         case 3: *gpio3_output_enable |= (1<<gpio_bit); break;
      }

      /** 
       * Tell the PRU unit that we are interested in input from this pin.
       * See comments in definitions.h
       */
      beaglebone_pruio_shared_ram[gpio_module+GPIO0_CONFIG] |= (1<<gpio_bit);
   }
 
   // fclose(f);
   return 0;
}

void beaglebone_pruio_set_pin_value(int gpio_number, int value){
   int gpio_module = gpio_number >> 5;
   int gpio_bit = gpio_number % 32;
   volatile unsigned int* reg=NULL;
   switch(gpio_module){
      case 0: reg = gpio0_data_out; break;
      case 1: reg = gpio1_data_out; break;
      case 2: reg = gpio2_data_out; break;
      case 3: reg = gpio3_data_out; break;
   }

   if(value==1){
      *reg |= (1<<gpio_bit);
   }
   else{
      *reg &= ~(1<<gpio_bit);
   }
}

int beaglebone_pruio_set_midi_channel(int channel) {
  // channel between 0 and 15
   if( channel<0 || channel>15) {
    fprintf(stderr, "libbeaglebone_pruio: MIDI channel must be between 0 and 15.\n");
    return 1;
  }

  beaglebone_pruio_shared_ram[MIDI_CONFIG] = channel & 0xF;
  return 0;
}

int beaglebone_pruio_stop(){
   // TODO: send terminate message to PRU

   prussdrv_pru_disable(0);
   prussdrv_pru_disable(1);
   prussdrv_exit();

   return 0;
}


