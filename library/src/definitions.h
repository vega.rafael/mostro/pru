/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DEFINITIONS_H
#define DEFINITIONS_H

// #include "beaglebone_pruio_pins.h"

/////////////////////////////////////////////////////////////////////
// Communication between ARM and PRU0 processors
//

/**
 * Messages from PRU0 to ARM processor are passed througn a 
 * ring buffer in the PRU shared memory area. Messages are 
 * 32 bit unsigned integers:
 * 
 * A GPIO Message:
 * 0XXX XXXX XXXX XXXX XXXX XXXV NNNN NNNN
 * |             |             |      |
 * |             |             |      |-- 7-0: GPIO number
 * |             |             |
 * |             |             |-- 8: Value
 * |             |         
 * |             |-- 30-9: Unused
 * |
 * |-- 31: Always 0, indicates this is a gpio message
 * 
 * 
 * 
 * An ADC Message:
 * 1XXX XXXX XXXX XXXX VVVV VVVV VVVV NNNN
 * |             |             |       |
 * |             |             |       |-- 3-0: ADC Channel
 * |             |             |
 * |             |             |-- 4-15: Value
 * |             |         
 * |             |-- 30-16: Unused
 * |
 * |-- 31: Always 1, indicates this is an adc message
 * 
 * Read these to understand the ring buffer:
 *  > http://en.wikipedia.org/wiki/Circular_buffer#Use_a_Fill_Count
 *  > https://groups.google.com/forum/#!category-topic/beagleboard/F9JI8_vQ-mE
 */
#define RING_BUFFER_0_ADDRESS 0
#define RING_BUFFER_0_SIZE 512
#define RING_BUFFER_0_START (RING_BUFFER_0_ADDRESS + RING_BUFFER_0_SIZE)
#define RING_BUFFER_0_END (RING_BUFFER_0_START + 1)


/////////////////////////////////////////////////////////////////////
// Communication from PRU1 to ARM processor
//

/**
 * Messages from PRU1 to ARM processor are passed througn a 
 * ring buffer in the PRU shared memory area.  Messages are 
 * 32 bit unsigned integers:
 * 
 * A 3 byte MIDI Message:
 * xxxx xxxd cccc cccc bbbb bbbb aaaa aaaa
 * |       |         |         |         |
 * |       |         |         |         |-- 0-7: byte 2
 * |       |         |         |
 * |       |         |         |-- 8-15: byte 1
 * |       |         |         
 * |       |         |-- 16-23: byte 0
 * |       |
 * |       |-- 24: 0x0 if this not is an interesting midi message 
 * |               0x1 if this is an interesting midi message 
 * |
 * |-- 28-31: Unused
 * 
 */
#define RING_BUFFER_1_ADDRESS (RING_BUFFER_0_END + 1)
#define RING_BUFFER_1_SIZE 512
#define RING_BUFFER_1_START (RING_BUFFER_1_ADDRESS + RING_BUFFER_1_SIZE)
#define RING_BUFFER_1_END (RING_BUFFER_1_START + 1)


/////////////////////////////////////////////////////////////////////
// Communication from ARM to PRU1 processor
//

/**
 * Messages from ARM to PRU1 processor are passed througn a 
 * ring buffer in the PRU shared memory area.  Messages are 
 * 8 bit unsigned integers (1 byte midi messages).
 * The 512 size is in 32 bit slots, since messaegs are only 8 
 * bits long, we actually have a 2M buffer.
 *
 */

#define RING_BUFFER_2_ADDRESS (RING_BUFFER_1_END + 1)
#define RING_BUFFER_2_SIZE 512
#define RING_BUFFER_2_SIZE_BYTES 512*4
#define RING_BUFFER_2_START (RING_BUFFER_2_ADDRESS + RING_BUFFER_2_SIZE)
#define RING_BUFFER_2_END (RING_BUFFER_2_START + 1)


/////////////////////////////////////////////////////////////////////
// Configuration of GPIO. Used by PRU0.
//
/*
 * Each one of the 32 bits in the following addresses represent the 32 
 * pins for the GPIO0 module. If a bit is set, it means that we need
 * input for that GPIO channel. Same mechanism is used for GPIO1, 2 and 3
 * using the following shared ram positions:
 */
#define GPIO0_CONFIG (RING_BUFFER_2_END + 1)
#define GPIO1_CONFIG (GPIO0_CONFIG + 1)
#define GPIO2_CONFIG (GPIO1_CONFIG + 1)
#define GPIO3_CONFIG (GPIO2_CONFIG + 1)


/////////////////////////////////////////////////////////////////////
// Configuration of ADC. Used by PRU0.
//
/**
 * The following addresses are options for each adc channel
 * Each configuration number is a 32 bit unsigned integer:
 * 
 * MMMM XXXX RRRR RRRR QQQQ QQQQ PPPP PPPP
 * |     |       |         |         |
 * |     |       |         |         |-- 7-0: Parameter 1
 * |     |       |         |
 * |     |       |         |-- 8-15: Parameter 2
 * |     |       |         
 * |     |       |-- 16-23: Parameter 3
 * |     |
 * |     |-- 24-27: Unused
 * |
 * |-- 31-28: Mode.
 * 
 * Mode 0: OFF. No input for this channel.
 * 
 * Mode 1: Normal mode. An adc value is passed to the arm code when it changes. 
 *         Parameter 1: Bit depth of the data (max 8)
 *         Parameter 2: Number of samples to use for moving average filter
 *                      NSamples = 2^M and the parameter we set here is M.
 */
#define ADC0_CONFIG (GPIO3_CONFIG + 1)
#define ADC1_CONFIG (ADC0_CONFIG + 1)
#define ADC2_CONFIG (ADC1_CONFIG + 1)
#define ADC3_CONFIG (ADC2_CONFIG + 1)
#define ADC4_CONFIG (ADC3_CONFIG + 1)
#define ADC5_CONFIG (ADC4_CONFIG + 1)
#define ADC6_CONFIG (ADC5_CONFIG + 1)
#define ADC7_CONFIG (ADC6_CONFIG + 1)
#define ADC8_CONFIG (ADC7_CONFIG + 1)
#define ADC9_CONFIG (ADC8_CONFIG + 1)
#define ADC10_CONFIG (ADC9_CONFIG + 1)
#define ADC11_CONFIG (ADC10_CONFIG + 1)
#define ADC12_CONFIG (ADC11_CONFIG + 1)
#define ADC13_CONFIG (ADC12_CONFIG + 1)

/////////////////////////////////////////////////////////////////////
// Configuration of MIDI messages. Used by PRU1
//
/*
 * The MIDI channel to use. It should only be set from the ARM side.
 */
#define MIDI_CONFIG (ADC13_CONFIG + 1)

/////////////////////////////////////////////////////////////////////
// History of data captured by ADC
//
/**
 * This is where we store the history of captured samples so we can 
 * calculate the moving average filter for the ADC data.
 * We have 14 channels of ADC, 8 bits per sample and we store 256 samples 
 * per channel. That's 3584 bytes of data in 896 32-bit slots.
 */
#define ADC_DATA_ADDRESS (MIDI_CONFIG + 1)
#define ADC_DATA_SIZE 896
#define ADC_DATA_SIZE_PER_CHANNEL 256   // In bytes. That's 64 32-bit slots.


/**
 * We still have shared_ram[2458] to shared_ram[3072] (614 slots, or 2456 bytes)
 * for something else.
 */


/////////////////////////////////////////////////////////////////////
// Register addresses
//

// PRU Module Registers
#define PRU_ICSS_CFG 0x26000
#define PRU_ICSS_CFG_SYSCFG 0x04

// IEP Module Registers
#define IEP 0x2e000
#define IEP_TMR_GLB_CFG 0x00
#define IEP_TMR_GLB_STS 0x04
#define IEP_TMR_COMPEN 0x08
#define IEP_TMR_CNT 0x0C
#define IEP_TMR_CMP_CFG 0x40
#define IEP_TMR_CMP_STS 0x44
#define IEP_TMR_CMP0 0x48
#define IEP_TMR_CMP1 0x4C

// Clock Module registers
#define CM_PER 0x44e00000
#define CM_PER_GPIO1_CLKCTRL 0xac
#define CM_PER_GPIO2_CLKCTRL 0xb0
#define CM_PER_GPIO3_CLKCTRL 0xb4
#define CM_PER_UART4_CLKCTRL 0x78
#define CM_WKUP 0x44e00400
#define CM_WKUP_GPIO0_CLKCTRL 0x08
#define CM_WKUP_ADC_TSK_CLKCTL 0xbc

// GPIO Module registers
#define GPIO0 0x44e07000
#define GPIO1 0x4804c000
#define GPIO2 0x481ac000
#define GPIO3 0x481ae000
#define GPIO_CTRL 0x130
#define GPIO_OE 0x134
#define GPIO_DATAIN 0x138
#define GPIO_DATAOUT 0x13c
#define GPIO_DEBOUNCENABLE 0x150
#define GPIO_DEBOUNCINGTIME 0x154
#define GPIO_CLEARDATAOUT 0x190
#define GPIO_SETDATAOUT 0x194

// ADC Registers
#define ADC_TSC 0x44e0d000
#define ADC_TSC_IRQSTATUS 0x28
#define ADC_TSC_IRQENABLE_SET 0x2c
#define ADC_TSC_CTRL 0x40
#define ADC_TSC_ADCRANGE 0x48
#define ADC_TSC_CLKDIV 0x4c
#define ADC_TSC_STEPENABLE 0x54
#define ADC_TSC_STEPCONFIG1 0x64
#define ADC_TSC_STEPDELAY1 0x68
#define ADC_TSC_STEPCONFIG2 0x6c
#define ADC_TSC_STEPDELAY2 0x70
#define ADC_TSC_STEPCONFIG3 0x74
#define ADC_TSC_STEPDELAY3 0x78
#define ADC_TSC_STEPCONFIG4 0x7c
#define ADC_TSC_STEPDELAY4 0x80
#define ADC_TSC_STEPCONFIG5 0x84
#define ADC_TSC_STEPDELAY5 0x88
#define ADC_TSC_STEPCONFIG6 0x8c
#define ADC_TSC_STEPDELAY6 0x90
#define ADC_TSC_STEPCONFIG7 0x94
#define ADC_TSC_STEPDELAY7 0x98
#define ADC_TSC_STEPCONFIG8 0x9c
#define ADC_TSC_STEPDELAY8 0xa0
#define ADC_TSC_STEPCONFIG9 0xa4
#define ADC_TSC_STEPDELAY9 0xa8
#define ADC_TSC_STEPCONFIG10 0xac
#define ADC_TSC_STEPDELAY10 0xb0
#define ADC_TSC_STEPCONFIG11 0xb4
#define ADC_TSC_STEPDELAY11 0xb8
#define ADC_TSC_STEPCONFIG12 0xbc
#define ADC_TSC_STEPDELAY12 0xc0
#define ADC_TSC_STEPCONFIG13 0xc4
#define ADC_TSC_STEPDELAY13 0xc8
#define ADC_TSC_STEPCONFIG14 0xcc
#define ADC_TSC_STEPDELAY14 0xd0
#define ADC_TSC_STEPCONFIG15 0xd4
#define ADC_TSC_STEPDELAY15 0xd8
#define ADC_TSC_STEPCONFIG16 0xdc
#define ADC_TSC_STEPDELAY16 0xe0
#define ADC_TSC_FIFO0COUNT 0xe4
#define ADC_TSC_FIFO1COUNT 0xf0
#define ADC_TSC_FIFO0DATA 0x100
#define ADC_TSC_FIFO1DATA 0x200

// UART4 Registers
#define UART4 0x481A8000
#define UART_THR 0x00
#define UART_RHR 0x00
#define UART_DLL 0x00
#define UART_SYSC 0x54
#define UART_SYSS 0x58
#define UART_LCR 0x0C
#define UART_FCR 0x08
#define UART_DLH 0x04
#define UART_IER_UART 0x04
#define UART_LSR_UART 0x14
#define UART_MDR1 0x20
#define UART_SSR 0x44

#endif //DEFINITIONS_H
