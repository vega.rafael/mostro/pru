/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "definitions.h"
#include "beaglebone_pruio_pins.h"

/////////////////////////////////////////////////////////////////////
// UTIL
//

#define HWREG(x) (*((volatile unsigned int *)(x)))

/////////////////////////////////////////////////////////////////////
// DECLARATIONS
//

volatile unsigned int* shared_ram;
volatile unsigned int* buffer_data;
/* volatile register unsigned int __R31; */


/////////////////////////////////////////////////////////////////////
// RING BUFFER
//

// Read the comments in definitions.h

unsigned int buffer_size;
volatile unsigned int *buffer_start;
volatile unsigned int *buffer_end;

void init_buffer(){
   buffer_data = &(shared_ram[RING_BUFFER_0_ADDRESS]);
   buffer_size = RING_BUFFER_0_SIZE; 
   buffer_start = &(shared_ram[RING_BUFFER_0_START]);
   buffer_end = &(shared_ram[RING_BUFFER_0_END]);
   *buffer_start = 0;
   *buffer_end = 0;
}

inline void buffer_write(unsigned int *message){
   // Note that if buffer is full, messages will be dropped
   unsigned int is_full = (*buffer_end == (*buffer_start^buffer_size)); // ^ is orex
   if(!is_full){
      buffer_data[*buffer_end & (buffer_size-1)] = *message;
      // Increment buffer end, wrap around 2*size
      *buffer_end = (*buffer_end+1) & (2*buffer_size - 1);
   }
}

/////////////////////////////////////////////////////////////////////
// GPIO and Mux Control
//

void init_gpio(){
   // TODO: * measure if hardware debounce mechanism adds latency.
   
   // See BeagleboneBlackP9HeaderTable.pdf from derekmolloy.ie
   // Way easier to read than TI's manual

   // Enable GPIO0 Module.
   HWREG(GPIO0 + GPIO_CTRL) = 0x00;
   // Enable clock for GPIO0 module. 
   HWREG(CM_WKUP + CM_WKUP_GPIO0_CLKCTRL) = (0x02) | (1<<18);
   // Set debounce time for GPIO0 module
   // time = (DEBOUNCINGTIME + 1) * 31uSec
   // HWREG(GPIO0 + GPIO_DEBOUNCINGTIME) = 255;

   // Enable GPIO1 Module.
   HWREG(GPIO1 + GPIO_CTRL) = 0x00;
   // Enable clock for GPIO1 module. 
   HWREG(CM_PER + CM_PER_GPIO1_CLKCTRL) = (0x02) | (1<<18);
   // Set debounce time for GPIO1 module
   // time = (DEBOUNCINGTIME + 1) * 31uSec
   /* HWREG(GPIO1 + GPIO_DEBOUNCINGTIME) = 255; */
   
   // Enable GPIO2 Module.
   HWREG(GPIO2 + GPIO_CTRL) = 0x00;
   // Enable clock for GPIO2 module. 
   HWREG(CM_PER + CM_PER_GPIO2_CLKCTRL) = (0x02) | (1<<18);
   // Set debounce time for GPIO2 module
   // time = (DEBOUNCINGTIME + 1) * 31uSec
   /* HWREG(GPIO2 + GPIO_DEBOUNCINGTIME) = 255; */
   
   // Enable GPIO3 Module.
   HWREG(GPIO3 + GPIO_CTRL) = 0x00;
   // Enable clock for GPIO3 module. 
   HWREG(CM_PER + CM_PER_GPIO3_CLKCTRL) = (0x02) | (1<<18);
   // Set debounce time for GPIO3 module
   // time = (DEBOUNCINGTIME + 1) * 31uSec
   /* HWREG(GPIO3 + GPIO_DEBOUNCINGTIME) = 255; */
}

inline void set_mux_control(unsigned int ctl){
   // 3 bits for mux control: 
   // < P8_29 GPIO2[23], P8_28 GPIO2[24], P8_27 GPIO2[22] >
   switch(ctl){
      case 0:
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<22);
         break;

      case 1:
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<22);
         break;

      case 2:
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<22);
         break;

      case 3:
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<22);
         break;

      case 4:
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<22);
         break;

      case 5:
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<22);
         break;

      case 6:
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) &= ~(1<<22);
         break;

      default: // 7
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<23);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<24);
         HWREG(GPIO2 + GPIO_DATAOUT) |= (1<<22);
         break;
   }
}

inline char * get_gpio_module_address(int module_number){
   int r;
   switch(module_number) {
      case 0:
         r = GPIO0; 
         break;
      case 1:
         r = GPIO1; 
         break;
      case 2:
         r = GPIO2; 
         break;
      case 3:
         r = GPIO3; 
         break;
   }
   return (char *)r;
}

/////////////////////////////////////////////////////////////////////
// TIMER
//
void init_iep_timer(){
   // We'll count 83.333 micro seconds with compare0 register and 45uSec
   // with compare1 register.
   // clock is 200MHz, use increment value of 5, 
   // compare values are then 83333 and 45000

   // 1. Initialize timer to known state
   // 1.1 Disable timer counter
   HWREG(IEP + IEP_TMR_GLB_CFG) &= ~(1); 
   // 1.2 Reset counter (write 1 to clear)
   HWREG(IEP + IEP_TMR_CNT) = 0xffffffff; 
   // 1.3 Clear overflow status
   HWREG(IEP + IEP_TMR_GLB_STS) = 0; 
   // 1.4 Clear compare status (write 1 to clear)
   HWREG(IEP + IEP_TMR_CMP_STS) = 0xf; 


   // 2. Set compare values 
   // HWREG(IEP + IEP_TMR_CMP0) = 40000; 
   // HWREG(IEP + IEP_TMR_CMP0) = 50000; 
   HWREG(IEP + IEP_TMR_CMP0) = 83333; 
   // HWREG(IEP + IEP_TMR_CMP0) = 166666; 
   // HWREG(IEP + IEP_TMR_CMP0) = 166666 * 2;

   // 2.1 Compare register 1 to 45000
   /* HWREG(IEP + IEP_TMR_CMP1) = 45000; // Used when debugging timing */ 

   // 3. Enable compare events and reset counter when 
   // compare 0 event happens
   HWREG(IEP + IEP_TMR_CMP_CFG) = (1 << 1) | 1; // Compare event 0 only
   /* HWREG(IEP + IEP_TMR_CMP_CFG) = (1 << 2) | (1 << 1) | 1; // Compare evts 0 and 1 */ 
   
   // 4. Set increment value (5)
   HWREG(IEP + IEP_TMR_GLB_CFG) |= 5<<4; 

   // 5. Set compensation value (not needed now)
   HWREG(IEP + IEP_TMR_COMPEN) = 0; 
   
   // 6. Enable counter
   HWREG(IEP + IEP_TMR_GLB_CFG) |= 1; 
}

inline void wait_for_timer(){
   // Wait for compare 0 status to go high
   while((HWREG(IEP+IEP_TMR_CMP_STS) & 1) == 0){
      // nothing 
   }

   // Clear compare 0 status (write 1)
   HWREG(IEP+IEP_TMR_CMP_STS) |= 1;
}

inline void wait_for_short_timer(){
   // Wait for compare 1 status to go high
   while((HWREG(IEP+IEP_TMR_CMP_STS) & (1<<1)) == 0){
      // nothing 
   }

   // Clear compare 1 status (write 1)
   HWREG(IEP+IEP_TMR_CMP_STS) |= (1<<1);
}

/////////////////////////////////////////////////////////////////////
// Analog Digital Conversion
//
inline void wait_for_adc(){
   // Wait for irqstatus[1] to go high
   while((HWREG(ADC_TSC + ADC_TSC_IRQSTATUS) & (1<<1)) == 0){
      // nothing 
   }

   // Clear status (write 1)
   HWREG(ADC_TSC + ADC_TSC_IRQSTATUS) |= (1<<1);
}

/*
inline void adc_start_sampling(){
   // Enable step 7
   // HWREG(ADC_TSC + ADC_TSC_STEPENABLE) = (1 << 7);

   // Enable steps 1 to 7
   HWREG(ADC_TSC + ADC_TSC_STEPENABLE) = 0xfe;
}
*/

void init_adc(){
   // Enable clock for adc module.
   HWREG(CM_WKUP + CM_WKUP_ADC_TSK_CLKCTL) = 0x02;

   // Disable ADC module temporarily.
   HWREG(ADC_TSC + ADC_TSC_CTRL) &= ~(0x01);

   // The theoretical sample rate is calculated by the following expression:
   // fs = 24MHz / (CLK_DIV*2*Channels*(OpenDly+Average*(14+SampleDly)))
   // It looks like since we're using one-shot configuration, the sampling becomes slower.
   // Experimentally, I measured a 77.42KHz sample rate for only one channel, and 12.01KHz 
   // for 7 channels. 12KHz is pretty good for our application.
   unsigned int clock_divider = 1;
   unsigned int open_delay = 0;
   unsigned int average = 0;       // can be 0 (no average), 1 (2 samples), 
                                   // 2 (4 samples),  3 (8 samples) 
                                   // or 4 (16 samples)
   unsigned int sample_delay = 0;

   // Set clock divider (set register to desired value minus one). 
   HWREG(ADC_TSC + ADC_TSC_CLKDIV) = clock_divider - 1;

   // Set values range from 0 to FFF.
   HWREG(ADC_TSC + ADC_TSC_ADCRANGE) = (0xfff << 16) & (0x000);

   // Disable all steps. STEPENABLE register
   HWREG(ADC_TSC + ADC_TSC_STEPENABLE) &= ~(0xff);

   // Unlock step config register.
   HWREG(ADC_TSC + ADC_TSC_CTRL) |= (1 << 2);

   // ATTENTION!!! Step and channel numbers are not consecutive (we want to sample the mux channel last)

   // Set config and delays for step 1: 
   // Sw mode, one shot mode, fifo0, channel 0.
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG1) = 0 | (0<<26) | (0<<19) | (0<<15) | (average<<2) | (0);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY1)  = 0 | (sample_delay - 1)<<24 | open_delay;

   // Set config and delays for step 2: 
   // Sw mode, one shot mode, fifo0, channel 1.
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG2) = 0 | (0x0<<26) | (0x01<<19) | (0x01<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY2)  = 0 | (sample_delay - 1)<<24 | open_delay;

   // Set config and delays for step 3: 
   // Sw mode, one shot mode, fifo0, channel 3.
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG3) = 0 | (0x0<<26) | (0x03<<19) | (0x03<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY3)  = 0 | ((sample_delay - 1)<<24) | open_delay;

   // Set config and delays for step 4: 
   // Sw mode, one shot mode, fifo0, channel 4.
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG4) = 0 | (0x0<<26) | (0x04<<19) | (0x04<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY4)  = 0 | ((sample_delay - 1)<<24) | open_delay;

   // Set config and delays for step 5: 
   // Sw mode, one shot mode, fifo0, channel 5.
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG5) = 0 | (0x0<<26) | (0x05<<19) | (0x05<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY5)  = 0 | ((sample_delay - 1)<<24) | open_delay;

   // Set config and delays for step 6: 
   // Sw mode, one shot mode, fifo0, channel 6
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG6) = 0 | (0x0<<26) | (0x06<<19) | (0x06<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY6)  = 0 | ((sample_delay - 1)<<24) | open_delay;

   // Set config and delays for step 7: 
   // Sw mode, one shot mode, fifo0, CHANNEL 2!
   HWREG(ADC_TSC + ADC_TSC_STEPCONFIG7) = 0 | (0x0<<26) | (0x02<<19) | (0x02<<15) | (average<<2) | (0x00);
   HWREG(ADC_TSC + ADC_TSC_STEPDELAY7)  = 0 | ((sample_delay - 1)<<24) | open_delay;

   // Enable tag channel id. Samples in fifo will have channel id bits ADC_CTRL register
   HWREG(ADC_TSC + ADC_TSC_CTRL) |= (1 << 1);

   // Clear End_of_sequence interrupt
   HWREG(ADC_TSC + ADC_TSC_IRQSTATUS) |= (1<<1);

   // Enable End_of_sequence interrupt
   HWREG(ADC_TSC + ADC_TSC_IRQENABLE_SET) |= (1 << 1);
   
   // Lock step config register. ACD_CTRL register
   HWREG(ADC_TSC + ADC_TSC_CTRL) &= ~(1 << 2);
   
   // Clear FIFO0 by reading from it.
   unsigned int count = HWREG(ADC_TSC + ADC_TSC_FIFO0COUNT);
   unsigned int data, i;
   for(i=0; i<count; i++){
      data = HWREG(ADC_TSC + ADC_TSC_FIFO0DATA);
   }

   // Clear FIFO1 by reading from it.
   count = HWREG(ADC_TSC + ADC_TSC_FIFO1COUNT);
   for(i=0; i<count; i++){
      data = HWREG(ADC_TSC + ADC_TSC_FIFO1DATA);
   }
   buffer_data[500] = data; // just remove unused value warning;

   // Enable ADC Module. ADC_CTRL register
   HWREG(ADC_TSC + ADC_TSC_CTRL) |= 1;
}

/////////////////////////////////////////////////////////////////////
// ANALYZE ADC VALUES 
//

// See comments for adc config in definitions.h
typedef struct adc_channel{
   int mode; 
   int first_time;
   
   unsigned int value;
   unsigned int value_1;
   unsigned int value_2;
   unsigned long int accumulator;
   size_t values_ptr;
   volatile unsigned char* values;

   unsigned int parameter1;
   unsigned int parameter2;
   unsigned int parameter3;
} adc_channel;

adc_channel adc_channels[BEAGLEBONE_PRUIO_MAX_ADC_CHANNELS];

unsigned int mux_control = 0;


inline void send_adc_value(unsigned int channel_number, unsigned int value, unsigned int bits) {
   unsigned int message;
   value = (value << (8-bits)) & 0xFF; // send with 8 bit scale
   message = ((unsigned int)1<<31) | (value<<4) | (channel_number);
   buffer_write(&message);
}

inline void process_adc_value(unsigned int channel_number, unsigned int value){
   adc_channel* channel = &(adc_channels[channel_number]);

   // Truncate data to n bits
   unsigned int bits = channel->parameter1;
   value = value >> (12-bits);

   // Moving average filter
   unsigned int average_size = channel->parameter3;
   unsigned int shift_places = channel->parameter2;
   unsigned int past = 0;
   if(channel->first_time) {
     size_t i;

     channel->value_2 = value;
     channel->value_1 = value;
     channel->value   = value;
     send_adc_value(channel_number, value, bits);

     channel->accumulator = 0;
     channel->values_ptr = 0;
     for(i=0; i<average_size; ++i) {
       channel->values[i] = value;  
       channel->accumulator = channel->accumulator + value;
     }
     channel->first_time = 0;
   }
   channel->values[channel->values_ptr] = value;
   channel->values_ptr++;
   channel->values_ptr %= average_size;
   past = channel->values[channel->values_ptr];
   channel->accumulator = channel->accumulator + value - past;
   value = (channel->accumulator >> shift_places);

   // Send the value to ARM. See message format definitions.h
   if(channel->value != value){
      
     // Another filter. naive implementation that works. 
     channel->value_2 = channel->value_1;
     channel->value_1 = channel->value;
     channel->value=value;
     if(channel->value != channel->value_1 && 
        channel->value != channel->value_2 &&
        channel->value_1 != channel->value_2) {
        send_adc_value(channel_number, value, bits);
     }
   }
}

// Read available samples from fifo0 in blocks of seven, figure out
// which channel they belong to (using step id and mux control) and
// send them to process_adc_value (singular) functions.
inline void process_adc_values(){
   unsigned int data, step_id, value, channel_number;
   unsigned int count = HWREG(ADC_TSC + ADC_TSC_FIFO0COUNT);
   while(count > 0){
     data = HWREG(ADC_TSC + ADC_TSC_FIFO0DATA);
     step_id = (data & (0x000f0000)) >> 16;
     value = (data & 0xfff);

     channel_number = step_id;
     if(step_id==6) {
       channel_number = 6+mux_control;
     }

     int mode = adc_channels[channel_number].mode;
     if(mode != 0) process_adc_value(channel_number, value);

     // if(mode != 0) {
     //   value = (value >> 4);
     //   unsigned int message = (1<<31) | (value<<4) | channel_number;
     //   buffer_write(&message);
     // }
     count = HWREG(ADC_TSC + ADC_TSC_FIFO0COUNT);
   }
}

void init_adc_values(){
   int i, j;
   adc_channel new_channel;

   for(i=0; i<BEAGLEBONE_PRUIO_MAX_ADC_CHANNELS; i++){
      new_channel.mode = 0;
      new_channel.first_time = 1;
      new_channel.parameter1 = 0;
      new_channel.parameter2 = 0;
      new_channel.parameter3 = 0;
      new_channel.accumulator = 0;
      new_channel.values_ptr = 0;
      new_channel.value = 2;
      new_channel.value_1 = 3;
      new_channel.value_2 = 4;

      volatile unsigned char* data_base_address = (volatile unsigned char*) &(shared_ram[ADC_DATA_ADDRESS]);
      new_channel.values = data_base_address + ADC_DATA_SIZE_PER_CHANNEL*i;
      for(j=0; j<ADC_DATA_SIZE_PER_CHANNEL; j++) {
         new_channel.values[j] = 0;
      }

      adc_channels[i] = new_channel; 
   }
}

inline void init_adc_channels(){
   /**
    * Checks if ARM code has requested input from new adc pins (channels)
    * and adds them to the adc_channels array. See comments in 
    * definitions.h
    */
   int i;
   unsigned int config = 0;
   int mode = 0;
   for(i=0; i<BEAGLEBONE_PRUIO_MAX_ADC_CHANNELS; i++){
      //not inited?
      if(adc_channels[i].mode == 0){
         config = shared_ram[ADC0_CONFIG+i];
         mode = ((config >> 28) & 0xF);
         if(mode != 0){
            adc_channels[i].mode = mode;
            adc_channels[i].parameter1 = config & 0xFF;
            adc_channels[i].parameter2 = (config >> 8) & 0xFF;
            /* adc_channels[i].parameter3 = (config >> 16) & 0xFF; */

            // Set the moving average filter pointer so it looks as if we filled the 
            // filter history with zeros for a long time. Store the average size in parameter 3
            unsigned int shift = adc_channels[i].parameter2;
            unsigned int average_window_size = 0x1 << shift;
            adc_channels[i].parameter3 = average_window_size;
            adc_channels[i].values_ptr = 0;
         }
      }
   }
}

/////////////////////////////////////////////////////////////////////
// ANALYZE GPIO VALUES 
//

typedef struct gpio_channel{
   int gpio_number; 
   unsigned int value; 
} gpio_channel;

gpio_channel gpio_channels[BEAGLEBONE_PRUIO_MAX_GPIO_CHANNELS];
int gpio_channel_count=0;

inline void process_gpio_values(){
   int i, module_number, pin;
   unsigned int new_value, message;
   char *module;
   gpio_channel *channel;

   for(i=0; i<gpio_channel_count; i++){
      channel = &gpio_channels[i];
      module_number = channel->gpio_number / 32; 
      pin = channel->gpio_number % 32;
      module = get_gpio_module_address(module_number);
      new_value = ((HWREG(module+GPIO_DATAIN)&(1<<pin)) != 0);

      if(channel->value != new_value){
         // See message format explanation in comments in ring buffer section
         message = (0<<31) | (new_value<<8) | channel->gpio_number; 
         buffer_write(&message);
         channel->value = new_value;
      }
   }
}

void init_gpio_values(){
   gpio_channel_count = 0;
}

inline void init_gpio_channel(int gpio_module, int bit){
   int gpio_number = gpio_module*32 + bit;

   // Check if channel is already initialized
   int i;
   for(i=0; i<gpio_channel_count; ++i){
      if(gpio_channels[i].gpio_number == gpio_number){
         return;      
      }
   }

   // Add the new channel to the gpio_channels array
   gpio_channel new_channel;
   new_channel.value = 2;
   new_channel.gpio_number = gpio_number;
   gpio_channels[gpio_channel_count] = new_channel;
   gpio_channel_count++;
}

inline void init_gpio_channels(){
   /**
    * Checks if ARM code has requested input from new gpio pins 
    * (channels) and adds them to the gpio_channels array. See
    * commens in definitions.h
    */
   int gpio_module;
   for(gpio_module=0; gpio_module<4; ++gpio_module){
      unsigned int config = shared_ram[GPIO0_CONFIG+gpio_module];
      int bit;
      for(bit=0; bit<32; ++bit){
         // If bit is set
         if((config&(1<<bit)) != 0){
            init_gpio_channel(gpio_module, bit); 
         }
      }
   }
}

/////////////////////////////////////////////////////////////////////
// MAIN
//

void init_ocp(){
   // Enable OCP so we can access the whole memory map for the
   // device from the PRU. Clear bit 4 of SYSCFG register
   HWREG(PRU_ICSS_CFG + PRU_ICSS_CFG_SYSCFG) &= ~(1 << 4);

   // Pointer to shared memory region
   shared_ram = (volatile unsigned int *)0x10000;
}

int main(int argc, const char *argv[]){
   init_ocp();
   init_buffer();
   init_adc();
   init_adc_values();
   init_gpio();
   init_gpio_values();
   init_iep_timer();

   // Debug:
   /* unsigned int i; */

   mux_control = 7;
   // TODO: exit condition
   unsigned int finished = 0;
   while(!finished){
      mux_control>6 ? mux_control=0 : mux_control++;
      set_mux_control(mux_control);

      // Sample ADC steps 1 through 6
      HWREG(ADC_TSC + ADC_TSC_STEPENABLE) = 0x7e;
      wait_for_adc();

      wait_for_timer(); // Timer resets itself after this

      // Sample ADC step 7
      HWREG(ADC_TSC + ADC_TSC_STEPENABLE) = (1 << 7);
      wait_for_adc();

      process_adc_values();
      process_gpio_values();

      init_gpio_channels();
      init_adc_channels();

      // Debug.  Output a pulse on pin GPIO1[28] (P9_12)
      // unsigned int i;
      // HWREG(GPIO1 + GPIO_DATAOUT) |= (1<<28);
      // for(i=0; i<20; i++); 
      // HWREG(GPIO1 + GPIO_DATAOUT) &= ~(1<<28);
      
      // Debug. Wait for adc to aquire data, read data to clear fifo.
      /* wait_for_adc(); */
      /* volatile unsigned int data; */
      /* volatile unsigned int count = HWREG(ADC_TSC + ADC_TSC_FIFO0COUNT); */
      /* while(count > 0){ */
      /*   /1* for(i=0; i<7; i++){ *1/ */
      /*     data = HWREG(ADC_TSC + ADC_TSC_FIFO0DATA); */
      /*   /1* } *1/ */
      /*   count = HWREG(ADC_TSC + ADC_TSC_FIFO0COUNT); */
      /* } */

      // Debug:
      /* wait_for_short_timer(); */
   }

   __halt();
   return 0;
}

