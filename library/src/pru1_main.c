/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "definitions.h"
#include "beaglebone_pruio_pins.h"

/////////////////////////////////////////////////////////////////////
// UTIL
//

#define HWREG(x) (*((volatile unsigned int *)(x)))

/////////////////////////////////////////////////////////////////////
// DECLARATIONS
//

volatile unsigned int* shared_ram;
volatile register unsigned int __R31;


/////////////////////////////////////////////////////////////////////
// GLOBALS
//

unsigned char running_status_byte = 0;

/////////////////////////////////////////////////////////////////////
// RING BUFFER 1. Read the comments in definitions.h
//
unsigned int buffer_size;
volatile unsigned int *buffer_data;
volatile unsigned int *buffer_start;
volatile unsigned int *buffer_end;

void init_buffer(){
   buffer_data = &(shared_ram[RING_BUFFER_1_ADDRESS]);
   buffer_size = RING_BUFFER_1_SIZE; 
   buffer_start = &(shared_ram[RING_BUFFER_1_START]);
   buffer_end = &(shared_ram[RING_BUFFER_1_END]);
   *buffer_start = 0;
   *buffer_end = 0;
}

inline void buffer_write(unsigned int *message){
   // Note that if buffer is full, messages will be dropped
   unsigned int is_full = (*buffer_end == (*buffer_start^buffer_size)); // ^ is orex
   if(!is_full){
      buffer_data[*buffer_end & (buffer_size-1)] = *message;
      // Increment buffer end, wrap around 2*size
      *buffer_end = (*buffer_end+1) & (2*buffer_size - 1);
   }
}

inline int get_midi_channel() {
  return shared_ram[MIDI_CONFIG];
}

/////////////////////////////////////////////////////////////////////
// RING BUFFER 2. Read the comments in definitions.h
//

unsigned int buffer2_size;
volatile unsigned char *buffer2_data;
volatile unsigned int *buffer2_start;
volatile unsigned int *buffer2_end;

void init_buffer2() {
   buffer2_size = RING_BUFFER_2_SIZE_BYTES;
   buffer2_data = (unsigned char*) &(shared_ram[RING_BUFFER_2_ADDRESS]);
   buffer2_start = &(shared_ram[RING_BUFFER_2_START]);
   buffer2_end = &(shared_ram[RING_BUFFER_2_END]);
   *buffer2_start = 0;
   *buffer2_end = 0;
}

inline int buffer2_read_available() {
   return (*buffer2_start != *buffer2_end);
}

inline void buffer2_read(unsigned char *message){
   *message = buffer2_data[*buffer2_start & (buffer2_size-1)];

   // Don't write buffer start before reading message (mem barrier)
   // http://stackoverflow.com/questions/982129/what-does-sync-synchronize-do
   // https://en.wikipedia.org/wiki/Memory_ordering#Compiler_memory_barrier
   // https://stackoverflow.com/questions/14950614/working-of-asm-volatile-memory
   /* asm volatile("" : : :"memory"); */

   // Increment buffer start, wrap around 2*size
   *buffer2_start = (*buffer2_start+1) & (2*buffer2_size - 1);
}

/////////////////////////////////////////////////////////////////////
// UART
//
void init_uart() {
  // Enable UART4 clock and wait for it.
  HWREG(CM_PER + CM_PER_UART4_CLKCTRL) = 0x2;
  while( ((HWREG(CM_PER+CM_PER_UART4_CLKCTRL)>>16)&0x3) != 0x0 ){}

  // Reset the UART4 module and wait for it
  HWREG(UART4 + UART_SYSC) = 0x2;
  while( (HWREG(UART4+UART_SYSS)&0x1) != 0x1 ){}

  // Set LCR[7]=0 (register configuration mode A) so we can write to 
  // a bunch of registers that will not be accessible otherwise. 
  // See manual 19.3.7.1. 
  HWREG(UART4 + UART_LCR) = 0x80;
  
  // Enable Tx and Rx FIFOs, clear them.
  HWREG(UART4 + UART_FCR) = (0x1<<2) | (0x1<<1) | 0x1;
  
  // Set baud rate to 31250.
  // Initial clock is 48MHz, divisor is <DLH, DLL> registers 
  // and an extra 16 divisor is used
  // 48M / 96 / 16 = 31250
  HWREG(UART4 + UART_DLL) = 0x60; // 96
  HWREG(UART4 + UART_DLH) = 0x00; 
     //  HWREG(UART4 + UART_MDR1) = 0x0; // 16x mode, don't set yet, keep uart off for now.

  // Serial format: 8 bits, 1 start bit, 1 stop bit, no parity
  HWREG(UART4 + UART_LCR) =  (0x3);

  // Set uart to USRT 16x mode (start running)
  HWREG(UART4 + UART_MDR1) = 0x0;
}

inline void uart_tx(unsigned char data) {
  // if tx fifo is full, wait
  while( (HWREG(UART4+UART_SSR)&0x1) == 0x1 ) {}

  // Write data
  HWREG(UART4 + UART_THR) = data;
}

inline int uart_rx(unsigned char* data) {
  int available = ( (HWREG(UART4 + UART_LSR_UART)&0x1) == 0x1 );
  if(available) {
    *data = HWREG(UART4 + UART_RHR);
    return 1;
  }
  else {
    return 0;
  }
}

void init_ocp(){
   // Enable OCP so we can access the whole memory map for the
   // device from the PRU. Clear bit 4 of SYSCFG register
   HWREG(PRU_ICSS_CFG + PRU_ICSS_CFG_SYSCFG) &= ~(1 << 4);

   // Pointer to shared memory region
   shared_ram = (volatile unsigned int *)0x10000;
}

/////////////////////////////////////////////////////////////////////
// MAIN
//
int current_midi_channel = 0;

#define INCOMING_MIDI_BYTES_SIZE 3
unsigned char incoming_midi_bytes[INCOMING_MIDI_BYTES_SIZE] = {0};
unsigned int count_incoming_midi_bytes = 0;
int expected_incoming_message_length = 255;
int incoming_message_is_interesting = 0;
int incoming_message_is_sysex = 0;
int incoming_message_channel = -1;

inline void process_midi_message(unsigned char* bytes, int n, int is_interesting) {
  unsigned char b2 = bytes[2];
  unsigned char b1 = bytes[1];
  unsigned char b0 = bytes[0];


  if(incoming_message_channel == -1 || current_midi_channel != incoming_message_channel) {
     int i = 0;
     for(i=0; i<n; i++) {
        uart_tx(bytes[i]);
     }
  }

  unsigned int message;
  is_interesting = is_interesting && current_midi_channel == incoming_message_channel;
  if(n == 3) {
     message = (is_interesting<<24) | (b0<<16) | (b1<<8) | (b2) | 0x0;
  }
  else if(n == 2) {
     message = (is_interesting<<24) | (b0<<16) | (b1<<8) | 0x0;
  }
  else if(n == 1) {
     message = (is_interesting<<24) | (b0<<16) | 0x0;
  }
  buffer_write(&message);
}

inline void process_midi_byte(unsigned char byte) {
  // 1. Take care of System real time messages. These messages are one byte only and 
  // can be interleaved with bytes from other messages.
  switch(byte) {
    case 0xF8: // Timing clock
    case 0xFA: // Start sequence
    case 0xFB: // Continue sequence
    case 0xFC: // Stop sequence
      process_midi_message(&byte, 1, 1);
      return;

    case 0xFE: // Active sensing
    case 0xFF: // Reset
    case 0xF9: // Undefined
    case 0xFD: // Undefined
      process_midi_message(&byte, 1, 0);
      return;
  }
  
  // 2. We don't really care about sysex messages so we won't wait until the end of
  //    a sysex message to process it (they could be really long). Just process each 
  //    byte individually. We still need to see when sysex messages start and end.
  switch(byte) {
    case 0xF0: // Start of sysex
      incoming_message_is_sysex = 1;
      process_midi_message(&byte, 1, 0);
      return;

    case 0xF7: // End of sysex
      incoming_message_is_sysex = 0;
      process_midi_message(&byte, 1, 0);
      return;
  }
  if(incoming_message_is_sysex) {
     process_midi_message(&byte, 1, 0);
     return;
  }

  // 3. First byte of System Common Messages. They start with 0xF and can be several bytes long. 
  // We'll figure out how many bytes to expect based on the message type. Put them incoming_midi_bytes[] 
  // and then send them to process_midi_message(). These messages don't have a channel number.
  int starts_with_f = ((byte & 0xF0)==0xF0);
  if(starts_with_f) {

    incoming_message_is_interesting = 0;
    incoming_message_channel = -1;

    int message_type = (byte & 0x0F);
    switch(message_type) {
      case 0x4: // Undefined
      case 0x5: // Undefined
      case 0x6: // Tune request
        process_midi_message(&byte, 1, 0);  //length is 1
        return;

      case 0x1: // time code quarter frame
      case 0x3: // Song select
        expected_incoming_message_length = 2;
        break;

      case 0x2: // Song position pointer
        expected_incoming_message_length = 3;
        break;
    }
    
    // Store byte and go on
    incoming_midi_bytes[0] = byte;
    count_incoming_midi_bytes = 1;
    return;
  }
  
  // 4. First byte of Channel Voice Messages. They start with a 1 can be several bytes long. 
  // We'll figure out how many bytes to expect based on the message type. Put them in 
  // incoming_midi_bytes[] and then send them to process_midi_message(). These messages DO have 
  // a channel number.
  int starts_with_one = ((byte & 0x80)==0x80);
  if(starts_with_one && !starts_with_f) {

    incoming_message_channel = (byte&0x0F);

    int message_type = (byte & 0xF0) >> 4;
    switch(message_type) {
      case 0x8: // Note Off 
      case 0x9: // Note On
      case 0xB: // Control Change / Channel mode messages
      case 0xE: // Pitch Bend
        expected_incoming_message_length = 3;
        incoming_message_is_interesting = 1;
        break;

      case 0xC: // Program change
        expected_incoming_message_length = 2;
        incoming_message_is_interesting = 1;
        break;

      case 0xA: // Aftertouch
      case 0xD: // Channel after touch 
      default:
        incoming_message_is_interesting = 0;
        expected_incoming_message_length = 3;
        break;
    }

    // Store byte and go on
    running_status_byte = byte;
    incoming_midi_bytes[0] = byte;
    count_incoming_midi_bytes = 1;
    return;
  }

  
  // 5. If we're supposed to get a start of message, but we didn't, we can assume the first byte is the same
  // as the one in the previous message. This is known as MIDI Running Status.
  // https://www.midikits.net/midi_analyser/running_status.htm
  if(count_incoming_midi_bytes==0 && !starts_with_one && !starts_with_f) {
    incoming_midi_bytes[0] = running_status_byte;
    count_incoming_midi_bytes = 1;
  }

  // 6. The second or third byte of a message arrived. Store it.
  incoming_midi_bytes[count_incoming_midi_bytes] = byte;
  count_incoming_midi_bytes++;
  if(count_incoming_midi_bytes > INCOMING_MIDI_BYTES_SIZE) {
    // This would be weird :/
    count_incoming_midi_bytes = 0;
  }

  // 7. This is the last byte, process the whole message.
  if(count_incoming_midi_bytes == expected_incoming_message_length) {
    process_midi_message(&incoming_midi_bytes[0], expected_incoming_message_length, incoming_message_is_interesting);
    count_incoming_midi_bytes = 0;
    return;
  }
}

int main(int argc, const char *argv[]){
   unsigned char received;

   init_ocp();
   init_buffer();
   init_buffer2();
   init_uart();

   unsigned int finished = 0;
   while(!finished){

      // If midi channel changed from ARM
      int channel = get_midi_channel();
      if(current_midi_channel != channel) {
        current_midi_channel = channel;

        // TODO: send all notes off message.
        
        // Debug. Send current midi channel
        // unsigned int message = (0x1<<24) | channel;
        // buffer_write(&message);

        // Debug send something
        // uart_tx(0x55);
      }

      // Midi in
      while(uart_rx(&received)) {
        // Debug. Send received to arm
        // unsigned int msg = received;
        // buffer_write(&msg);

        // Debug. Send each midi byte independently to arm side
        // unsigned int msg = (0x1 << 24 ) | received;
        // buffer_write(&msg);

        // Midi in
        process_midi_byte(received);
      }

      // Midi out
      unsigned char message = 0;
      while( buffer2_read_available() ) {
         buffer2_read(&message);
         uart_tx(message);
      }
      
      
      // Debug.  Output a pulse on pin GPIO1[18] (P9_14)
      // unsigned int i;
      // HWREG(GPIO1 + GPIO_DATAOUT) |= ((unsigned)1<<18);
      // for(i=0; i<20; i++); 
      // HWREG(GPIO1 + GPIO_DATAOUT) &= ~(1<<18);
      // for(i=0; i<10000; i++); 
   }

   __halt();
   return 0;
}


