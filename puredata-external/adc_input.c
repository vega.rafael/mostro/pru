/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <m_pd.h>
#include <stdio.h>
#include <string.h>
#include <beaglebone_pruio_pins.h>

#ifdef IS_BEAGLEBONE
#include <beaglebone_pruio.h>
#endif 

#include "beaglebone.h"

/////////////////////////////////////////////////////////////////////////
// Data
//

typedef struct adc_input {
   t_object x_obj;
   t_outlet *outlet_left;
   int channel;
   int bits;
   int average_length;
} t_adc_input;

// A pointer to the class object.
t_class *adc_input_class;

/////////////////////////////////////////////////////////////////////////
// Callback from lib beaglebone_pruio
//

void adc_input_callback(void* x, float value){
   t_adc_input* this = (t_adc_input*)x;
   outlet_float(this->outlet_left, value);
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void *adc_input_new(t_symbol *s, int argc, t_atom *argv) {
   (void)s;

   // 1.1 Parse channel number from creation parameters
   if(argc < 1){
      error("beaglebone/adc_input: You need to specify the channel number");
      return NULL;
   }
   
   // 1.1.1 Invalid floats will be parsed as zero so let's check if 
   //      that zero is actually valid
   float f = atom_getfloat(argv); // Argument number 0
   if( f==0 && strcmp("float", atom_getsymbol(argv)->s_name)!=0 ){
      error("beaglebone/adc_input: %s is not a valid ADC channel.", atom_getsymbol(argv)->s_name);
      return NULL;
   }

   // 1.1.2. Is channel number in range?
   if(f<0 || f>=BEAGLEBONE_PRUIO_MAX_ADC_CHANNELS || (float)((int)f)!=(f)){
      error("beaglebone/adc_input: %s is not a valid ADC channel.", atom_getsymbol(argv)->s_name); 
      return NULL;
   }

   // Are there more parameters? 
   int bits = 8;
   int average = 128;
   if(argc == 5){ 
      char* param2 = atom_getsymbol(argv+2)->s_name;
      char* param4 = atom_getsymbol(argv+4)->s_name;
      
      // 1.2 Parse number of bits
      if(strcmp(param2, "bits") == 0){
         bits = atom_getfloat(argv+3);
         if(bits<=0){
            bits = 7;
         }
         if(bits>8){
            bits = 8;
         }

         // Debug
         /* error("bits: %i", bits); */
      }

      // 1.3 Parse average size
      else if(strcmp(param4, "average") == 0){
         average = atom_getfloat(argv+5);
         
         // Debug
         /* error("Average: %i", average); */
      }
   }
   
   // 2. Try to initialize the adc input
   #ifdef IS_BEAGLEBONE
      if(beaglebone_pruio_init_adc_pin((int)f, bits, average)){
        error("beaglebone/adc_input: Could not init adc channel %s (%f), is it already in use?", atom_getsymbol(argv)->s_name, f);
        return NULL;
      }
   #endif 

   // 3. Create pd object instance
   t_adc_input *x = (t_adc_input *)pd_new(adc_input_class);
   x->outlet_left = outlet_new(&x->x_obj, &s_float);
   x->channel = (int)f;
   x->bits = bits;
   x->average_length = average;

   // 4. Register the callback.
   if(beaglebone_register_callback(BB_CALLBACK_ANALOG, (int)f, x, adc_input_callback)){
      error("beaglebone/adc_input: Could not init adc channel %s (%f), is it already in use?", atom_getsymbol(argv)->s_name, f);
      return NULL;
   }

   return (void *)x;
}

static void adc_input_free(t_adc_input *x) { 
   beaglebone_unregister_callback(BB_CALLBACK_ANALOG, x->channel);
}

/////////////////////////////////////////////////////////////////////////
// Class definition
// 

void adc_input_setup(void) {
   adc_input_class = class_new(
      gensym("adc_input"), 
      (t_newmethod)adc_input_new, 
      (t_method)adc_input_free, 
      sizeof(t_adc_input), 
      CLASS_NOINLET, 
      A_GIMME,
      (t_atomtype)0
   );
}
