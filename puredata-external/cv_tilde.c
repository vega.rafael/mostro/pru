/* MOSTRO
 *
 * Copyright (C) 2019 Rafael Vega <contacto@rafaelvega.co>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <m_pd.h>
#include <stdbool.h>
/* #include <stdio.h> */
/* #include <string.h> */

// forward declarations.
typedef struct t_cv_tilde t_cv_tilde; 
static void cv_tilde_timer(t_cv_tilde *x);

/////////////////////////////////////////////////////////////////////////
// Data
//

/* Value of gate input when unplugged */
#define GATE_INACTIVE (-0.66)

/* Thresshold above which the gate signal is considered high */
#define GATE_HI (-0.44)

/* Thresshold below which the gate signal is considered low */
#define GATE_LO (-0.96)

/* Period of the thimer to check if gate signal is inactive. milliseconds */
#define TIMER_PERIOD 1000

enum cv_tilde_state {
  STARTING = 0,
  INACTIVE,
  ACTIVE_LO,
  ACTIVE_HI //
};
typedef enum cv_tilde_state cv_tilde_state;

struct t_cv_tilde {
  t_object x_obj;
  float f;

  cv_tilde_state state;
  t_clock *clk;
  t_sample gate_values[2];
  bool seems_inactive;

  /*
  t_inlet* in_gate;   // left inlet is implicit
  */
  t_inlet *in_pitch;
  t_inlet *in_index;
  t_inlet *in_ratio;
  t_inlet *in_feedback;

  t_outlet *out_note_on;
  t_outlet *out_note_off;
  t_outlet *out_pitch;
  t_outlet *out_index;
  t_outlet *out_ratio;
  t_outlet *out_feedback;
  t_outlet *out_active;
};

// A pointer to the class object.
t_class *cv_tilde_class;

/////////////////////////////////////////////////////////////////////////
// DSP
//

// returns midi number for a signal value between -1 and 1.
// This table lists measured voltages, measured digital values, 
// expected midi number outputs and slopes of the line between 
// digital values and the midi numbers.
// We're doing linear interpolation between measured voltages.
// 
// Volts	Digital	        MIDI    freq		
// 0 - 2        <= -0.735       12      16.3516
// 3            -0.5            24      32.7032
// 3	        -0.272	        36	65.4064	        
// 4	        -0.041 	        48	130.813	        
// 5	        0.19	        60	261.626	        
// 6	        0.421	        72	523.251	        
// 7	        0.652	        84	1046.5	        
// 8 - 10       >= 0.884        96	2093	        
static inline t_sample cv_tilde_calculate_pitch(t_sample digital_value) {
  t_sample x = digital_value;
  t_sample digital_values[] = { -0.735, -0.5, -0.272, -0.041, 0.19, 0.421, 0.652, 0.884 };
  t_sample freqs[] = { 16.3516, 32.7032, 65.4064, 130.813, 261.626, 523.251, 1046.5, 2093 };

  unsigned int i = 0;
  for(i=0; i<sizeof(digital_values); ++i) {
    if(x <= digital_values[i]) {
      break;
    }
  }

  if(i==0) {
    return freqs[0];
  }
  else if(i == sizeof(digital_values)-1) {
    return freqs[ sizeof(freqs)-1 ];
  }
  else {
    t_sample x1 = digital_values[i-1];
    t_sample x2 = digital_values[i];
    t_sample y1 = freqs[i-1];
    t_sample y2 = freqs[i];

    t_sample y = y1 + (x - x1) * (y2 - y1) / (x2 - x1);
    return y;
  }
}

t_int *cv_tilde_perform(t_int *w) {
  t_cv_tilde *x = (t_cv_tilde *)(w[1]);
  t_sample *in_gate = (t_sample *)(w[2]);
  t_sample *in_pitch = (t_sample *)(w[3]);
  t_sample *out_pitch = (t_sample *)(w[4]);
  int n = (int)(w[5]);
  t_int* ret = w + 6;

  if(x->state == STARTING) {
    return ret;
  }    


  // Store some values of the gate signal, they'll be analyzed in the timer
  // function to see if gate is unplugged (inactive)
  x->gate_values[0] = in_gate[0];
  x->gate_values[1] = in_gate[n / 2];

  // Detect note ons and note offs from gate signal
  for (int i = 0; i < n; ++i) {
    t_sample gate_in = in_gate[i];
    if (x->state != ACTIVE_HI && gate_in >= GATE_HI) {
      if (x->state == INACTIVE) {
        outlet_float(x->out_active, 1.0);
      }
      outlet_bang(x->out_note_on);
      x->state = ACTIVE_HI;
      x->seems_inactive = false;
    } else if (x->state != ACTIVE_LO && gate_in <= GATE_LO) {
      if (x->state == INACTIVE) {
        outlet_float(x->out_active, 1.0);
      }
      outlet_bang(x->out_note_off);
      x->state = ACTIVE_LO;
      x->seems_inactive = false;
    }
  }

  // Convert pitch signal input (volts) to adequate range (midi numbers)
  if (x->state == INACTIVE || x->state == STARTING) {
    for (int i = 0; i < n; ++i) {
      out_pitch[i] = 0.0;
    }
  }
  else {
    for (int i = 0; i < n; ++i) {
      t_sample in_p = in_pitch[i];
      out_pitch[i] = cv_tilde_calculate_pitch(in_p);
    }
  }

  return ret;
}

void cv_tilde_dsp(t_cv_tilde *x, t_signal **sp) {
  x->f = GATE_INACTIVE;
  x->state = STARTING;
  outlet_float(x->out_active, 0.0);

  x->gate_values[0] = GATE_INACTIVE;
  x->gate_values[1] = GATE_INACTIVE;
  x->seems_inactive = true;

  x->clk = clock_new(x, (t_method)cv_tilde_timer);
  clock_delay(x->clk, TIMER_PERIOD);

  dsp_add(cv_tilde_perform, 5, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec,
          sp[0]->s_n);
}

/////////////////////////////////////////////////////////////////////////
// Timer
//

static void cv_tilde_timer(t_cv_tilde *x) {
  t_sample v1 = x->gate_values[0];
  t_sample v2 = x->gate_values[1];

  if(x->state == STARTING) {
    outlet_float(x->out_active, 0.0);
    x->state = INACTIVE;
  }

  bool seems_inactive_previous = x->seems_inactive;
  if (x->state != INACTIVE 
      && v1 > GATE_LO && v1 < GATE_HI 
      && v2 > GATE_LO && v2 < GATE_HI) {
    if (seems_inactive_previous) {
      outlet_float(x->out_active, 0);
      x->state = INACTIVE;
    }
    x->seems_inactive = true;
  }

  clock_delay(x->clk, TIMER_PERIOD);
}

/////////////////////////////////////////////////////////////////////////
// Scalar/control inputs
//

void cv_tilde_index(t_cv_tilde *x, t_floatarg f) {
  if (x->state != INACTIVE && x->state!=STARTING) {
    outlet_float(x->out_index, 1.0 - f);
  }
}

void cv_tilde_ratio(t_cv_tilde *x, t_floatarg f) {
  if (x->state != INACTIVE && x->state!=STARTING) {
    outlet_float(x->out_ratio, 1.0 - f);
  }
}

void cv_tilde_feedback(t_cv_tilde *x, t_floatarg f) {
  if (x->state != INACTIVE && x->state!=STARTING) {
    outlet_float(x->out_feedback, 1.0 - f);
  }
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void *cv_tilde_new(void) {
  // Create pd object instance
  t_cv_tilde *x = (t_cv_tilde *)pd_new(cv_tilde_class);

  x->in_pitch = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  x->in_index =
      inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_float, gensym("index"));
  x->in_ratio =
      inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_float, gensym("ratio"));
  x->in_feedback =
      inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_float, gensym("feedback"));

  x->out_note_on = outlet_new(&x->x_obj, &s_bang);
  x->out_note_off = outlet_new(&x->x_obj, &s_bang);
  x->out_pitch = outlet_new(&x->x_obj, &s_signal);
  x->out_index = outlet_new(&x->x_obj, &s_float);
  x->out_ratio = outlet_new(&x->x_obj, &s_float);
  x->out_feedback = outlet_new(&x->x_obj, &s_float);
  x->out_active = outlet_new(&x->x_obj, &s_float);


  return (void *)x;
}

static void cv_tilde_free(t_cv_tilde *x) {
  clock_free(x->clk);
  outlet_free(x->out_note_on);
  outlet_free(x->out_note_off);
  outlet_free(x->out_pitch);
  outlet_free(x->out_index);
  outlet_free(x->out_ratio);
  outlet_free(x->out_feedback);
  outlet_free(x->out_active);

  inlet_free(x->in_pitch);
  inlet_free(x->in_index);
  inlet_free(x->in_ratio);
  inlet_free(x->in_feedback);
  (void)x;
}

/////////////////////////////////////////////////////////////////////////
// Class definition
//

void cv_tilde_setup(void) {
  cv_tilde_class =
      class_new(gensym("cv~"), (t_newmethod)cv_tilde_new,
                (t_method)cv_tilde_free, sizeof(t_cv_tilde), CLASS_DEFAULT, 0);

  class_addmethod(cv_tilde_class, (t_method)cv_tilde_dsp, gensym("dsp"), A_CANT,
                  0);
  CLASS_MAINSIGNALIN(cv_tilde_class, t_cv_tilde, f);

  class_addmethod(cv_tilde_class, (t_method)cv_tilde_index, gensym("index"),
                  A_DEFFLOAT, 0);
  class_addmethod(cv_tilde_class, (t_method)cv_tilde_ratio, gensym("ratio"),
                  A_DEFFLOAT, 0);
  class_addmethod(cv_tilde_class, (t_method)cv_tilde_feedback,
                  gensym("feedback"), A_DEFFLOAT, 0);
}
