/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <m_pd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>

/*
 * This object drives a 2 character, 7 led display using a MCP23017 
 * GPIO expander connected with the I2C-1 bus in a beagle bone black.
 * The GPIO expander has 16 i/o pins that are connected to the display
 * leds as follows (numbers are bit numbers).
 *
 *     15          4 
 *   -----       -----  
 * 11| 10|14    0| 1 |5
 *   -----       -----   
 *  9|   |13    2|   |6 
 *   -----  o    -----  o 
 *     8   12      3    7
 *
 * The protocol for writing data to the GPIO expander throught i2C is 
 * dead simple: send the address of the register you want to write
 * followed by the one byte data, the address pointer is incremented
 * automatically and then send another byte to be written to address+1.
 */    

/////////////////////////////////////////////////////////////////////////
// Constants
//

#define SLAVE_ADDRESS 0x20u
#define REGISTER_ADDRESS 0x12

/////////////////////////////////////////////////////////////////////////
// Data
//

static int display_7_led_i2c_file_descriptor = 0;

typedef struct display_7_led {
   t_object x_obj;
   uint16_t current_state;
   bool is_on;
} t_display_7_led;

// A pointer to the class object.
t_class *display_7_led_class;


/////////////////////////////////////////////////////////////////////////
// Utility
//
// static int display_7_led_load_device_tree_overlay(char* dto){
//    // Check if the device tree overlay is loaded, load if needed.
//    int device_tree_overlay_loaded = 0; 
//    FILE* f;
//    f = fopen("/sys/devices/platform/bone_capemgr/slots","rt");
//    if(f==NULL){
//       return 1;
//    }
//    char line[256];
//    while(fgets(line, 256, f) != NULL){
//       if(strstr(line, dto) != NULL){
//          device_tree_overlay_loaded = 1; 
//       }
//    }
//    fclose(f);

//    if(!device_tree_overlay_loaded){
//       f = fopen("/sys/devices/platform/bone_capemgr/slots","w");
//       if(f==NULL){
//          return 1;
//       }
//       fprintf(f, "%s", dto);
//       fclose(f);
//    }

//    return 0;
// }

uint8_t display_7_led_char_to_led(char c, int is_left) {
  // First we convert the character to a 7bit led representation which corresponds to
  // our leds on the right.
  uint8_t leds = 0;
  switch(c) {
    case '0': leds=125; break;
    case '1': leds=96; break;
    case '2': leds=62; break;
    case '3': leds=122; break;
    case '4': leds=99; break;
    case '5': leds=91; break;
    case '6': leds=95; break;
    case '7': leds=112; break;
    case '8': leds=127; break;
    case '9': leds=123; break;

    case 'a': case 'A': leds=119; break;
    case 'b': case 'B': leds=79; break;
    case 'c': case 'C': leds=29; break;
    case 'd': case 'D': leds=110; break;
    case 'e': case 'E': leds=31 ; break;
    case 'f': case 'F': leds=23 ; break;
    case 'g': case 'G': leds=93 ; break;
    case 'h': case 'H': leds=71 ; break;
    case 'i': case 'I': leds=5  ; break;
    case 'j': case 'J': leds=108; break;
    case 'k': case 'K': leds= 87; break;
    case 'l': case 'L': leds=13 ; break;
    case 'm': case 'M': leds=86 ; break;
    case 'n': case 'N': leds=117; break;
    case 'o': case 'O': leds=125; break;
    case 'p': case 'P': leds=55 ; break;
    case 'q': case 'Q': leds=115; break;
    case 'r': case 'R': leds=53 ; break;
    case 's': case 'S': leds=91 ; break;
    case 't': case 'T': leds=15 ; break;
    case 'u': case 'U': leds=109; break;
    case 'v': case 'V': leds=76 ; break;
    case 'w': case 'W': leds=41 ; break;
    case 'x': case 'X': leds=103; break;
    case 'y': case 'Y': leds=107; break;
    case 'z': case 'Z': leds=58 ; break;

    case '#': leds=51; break;
    default: leds=0; break;
  }
  if(!is_left) {
    return leds;
  }

 /*     7          4 
 *   -----       -----  
 *  3| 2 |6    0| 1 |5
 *   -----       -----   
 *  1|   |5    2|   |6 
 *   -----  o    -----  o 
 *     0   4      3    7
 */
  // Then, if we need the left display, we map the pinout.
  uint8_t ret = 0;
  ret |= ((leds & 0x40) ? 1 : 0) << 5;    
  ret |= ((leds & 0x20) ? 1 : 0) << 6;    
  ret |= ((leds & 0x10) ? 1 : 0) << 7;    
  ret |= ((leds & 0x08) ? 1 : 0) << 0;    
  ret |= ((leds & 0x04) ? 1 : 0) << 1;    
  ret |= ((leds & 0x02) ? 1 : 0) << 2;   // map input bit 1 (mask 0x02) to output bit 2
  ret |= ((leds & 0x01) ? 1 : 0) << 3;   // map input bit 0 (mask 0x01) to output bit 3
  return ret;
}

char display_7_led_number_to_char(unsigned int n) {
  switch (n) {
    case 0: return '0';
    case 1: return '1';
    case 2: return '2';
    case 3: return '3';
    case 4: return '4';
    case 5: return '5';
    case 6: return '6';
    case 7: return '7';
    case 8: return '8';
    case 9: return '9';
    default: return '0';
  }
}

/////////////////////////////////////////////////////////////////////////
// On Message received
//

void display_7_led_on(t_display_7_led* x, t_floatarg f){
   if(f == x->is_on) return;

   if(f!=0 && f!=1){
      error("beaglebone/display_7_led: %f is not a correct value, only 0 and 1 allowed.", f);
      return;
   }

   x->is_on = f;

   #ifdef IS_BEAGLEBONE
     if(f==0){
       // Turn all leds off, write 0s to device register
       uint8_t buf[3] = {REGISTER_ADDRESS, 0x0, 0x0};
       write(display_7_led_i2c_file_descriptor, buf, 3);
     }
     else{
       // Display stored value
       uint16_t msg = x->current_state;
       uint8_t msb = msg >> 8;
       uint8_t lsb = msg & 0xFF;
       uint8_t buf[3] = {REGISTER_ADDRESS, msb, lsb};
       write(display_7_led_i2c_file_descriptor, buf, 3);
     }
   #else
      (void)x;
   #endif 
}

/////////////////////////////////////////////////////////////////////////
// DOT Message received
//

void display_7_led_dot(t_display_7_led* x, t_floatarg f1, t_floatarg f2){
   if((f1!=0 && f1!=1) || (f2!=0 && f2!=1)){
      error("beaglebone/display_7_led: dot only allows 0 0, 0 1, 1 0 or 1 1 values.");
      return;
   }


   uint16_t state = x->current_state;
   if(f1==1.0) {
     state |= (1<<12);
   }
   else {
     state &= ~(1<<12);
   }

   if(f2==1.0) {
     state |= (1<<7);
   }
   else {
     state &= ~(1<<7);
   }

   if(state != x->current_state) {
     x->current_state = state;
     uint8_t msb = state >> 8;
     uint8_t lsb = state & 0xFF;
     uint8_t buf[3] = {REGISTER_ADDRESS, msb, lsb};
     write(display_7_led_i2c_file_descriptor, buf, 3);
   }
}

/////////////////////////////////////////////////////////////////////////
// Number Message received
//

void display_7_led_number(t_display_7_led* x, t_floatarg f){
  (void)f;
   if(f<0 || f>99){
      error("beaglebone/display_7_led: %f is not a correct value, only integers between 0 and 99 are allowed.", f);
      return;
   }

   uint16_t dots = x->current_state & ((1<<12) | (1<<7));
   unsigned int n = f;
   unsigned int tens = n / 10;
   unsigned int units = n % 10;
   uint8_t t = display_7_led_number_to_char(tens);
   uint8_t u = display_7_led_number_to_char(units);
   uint8_t msb = display_7_led_char_to_led(t, 1);
   uint8_t lsb = display_7_led_char_to_led(u, 0);
   uint16_t state = (msb << 8) | lsb | dots;

   if(state != x->current_state) {
     x->current_state = state;
     uint8_t buf[3] = {REGISTER_ADDRESS, (state>>8), (state&0xFF)};
     write(display_7_led_i2c_file_descriptor, buf, 3);
   }
}

/////////////////////////////////////////////////////////////////////////
// Letter Message received
//

void display_7_led_letter(t_display_7_led* x, t_symbol* s, int argc, t_atom *argv){
  (void) s;

  if(argc == 0) {
    error("beaglebone/display_7_led: must pass a 2 char string to display letters\n");
    return;
  }

  char str[17];
  atom_string(&argv[0], &str[0], 16);
  if(strlen(str) > 2) {
    error("beaglebone/display_7_led: string to display can only have 2 characters\n");
    return;
  }

  uint16_t dots = x->current_state & ((1<<12) | (1<<7));
  char right = str[1];
  char left = str[0];
  uint8_t msb = display_7_led_char_to_led(left, 1);
  uint8_t lsb = display_7_led_char_to_led(right, 0);
  uint16_t state = (msb << 8) | lsb | dots;

  if(state != x->current_state) {
    x->current_state = state;
    uint8_t buf[3] = {REGISTER_ADDRESS, (state>>8), (state&0xFF)};
    write(display_7_led_i2c_file_descriptor, buf, 3);
  }
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void *display_7_led_new(){
  #ifdef IS_BEAGLEBONE
    if(display_7_led_i2c_file_descriptor == 0){
      // Load device tree overlay to turn i2c-1 hardware on and configure
      // P9_17 and p9_18 pins.
      // if(display_7_led_load_device_tree_overlay("BB-I2C1")){
      //   error("beaglebone/display_7_led: Could not load device tree overlay for i2c-1 device.");
      //   return NULL;
      // }
      // usleep(100000);

      // Open the file that represents the i2c device
      char *filename = "/dev/i2c-1";
      if((display_7_led_i2c_file_descriptor = open(filename, O_RDWR)) < 0){
        error("beaglebone/display_7_led: Failed to open the i2c bus: %s\n", strerror(errno));
        return NULL;
      }

      // Set slave device address.
      if(ioctl(display_7_led_i2c_file_descriptor, I2C_SLAVE, SLAVE_ADDRESS) < 0){
        error("beaglebone/display_7_led: Failed to acquire bus access and/or talk to slave: %s\n", strerror(errno));
        return NULL;
      }
      
      // Start talking to slave device. Set IO ports to output. 
      // Write two bytes to address 0x00 and 0x01 (pointer auto increments)
      uint8_t msg[3] = {0x00, 0x00, 0x00};
      write(display_7_led_i2c_file_descriptor, msg, 3);
    }
  #endif

  t_display_7_led *x = (t_display_7_led *)pd_new(display_7_led_class);
  x->current_state = 0;
  x->is_on = 0;
  display_7_led_number(x, 0);
  display_7_led_on(x, 1);

  return x;
}

static void display_7_led_free(t_display_7_led *x) { 
  close(display_7_led_i2c_file_descriptor);
  display_7_led_i2c_file_descriptor = 0;
  (void)x;
}

/////////////////////////////////////////////////////////////////////////
// Class definition
// 

void display_7_led_setup(void) {
   display_7_led_class = class_new(
      gensym("display_7_led"), 
      (t_newmethod)display_7_led_new, 
      (t_method)display_7_led_free, 
      sizeof(t_display_7_led), 
      CLASS_DEFAULT, 
      (t_atomtype)0
   );

   // Trigger display_7_led_on() when a "on" message is received
   class_addmethod(display_7_led_class, (t_method)display_7_led_on, gensym("on"), A_DEFFLOAT, 0);
   
   // Trigger display_7_led_dot() when a "dot" message is received
   class_addmethod(display_7_led_class, (t_method)display_7_led_dot, gensym("dot"), A_DEFFLOAT, A_DEFFLOAT, 0);
   
   // Trigger display_7_led_number() when a "number" message is received
   class_addmethod(display_7_led_class, (t_method)display_7_led_number, gensym("number"), A_DEFFLOAT, 0);
   
   // Trigger display_7_led_letter() when a "letter" message is received
   class_addmethod(display_7_led_class, (t_method)display_7_led_letter, gensym("letter"), A_GIMME, 0);
}
