/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <m_pd.h>
/* #include <stdio.h> */
/* #include <string.h> */
/* #include <beaglebone_pruio_pins.h> */

#ifdef IS_BEAGLEBONE
#include <beaglebone_pruio.h>
#endif 

#include "beaglebone.h"

/////////////////////////////////////////////////////////////////////////
// Data
//

typedef struct midi_in {
   t_object x_obj;
   t_outlet *outlet_left;
   t_outlet *outlet_right;
   unsigned char channel;
} t_midi_in;

// A pointer to the class object.
t_class *midi_in_class;

/////////////////////////////////////////////////////////////////////////
// Callback from lib beaglebone_pruio
//

void midi_in_callback(void* x, beaglebone_pruio_midi_message message){
   t_midi_in* this = (t_midi_in*)x;
   t_atom list[3];
   SETFLOAT(&list[0], (float)(message.byte0)); // message_type
   SETFLOAT(&list[1], (float)(message.byte1)); 
   SETFLOAT(&list[2], (float)(message.byte2)); 

   outlet_list(this->outlet_right, gensym("list"), 3, &list[0]);

   /* unsigned char channel = message.byte0 & 0x0F; */
   /* if(this->channel==channel && message.is_interesting) { */
   if(message.is_interesting) {
      // remove channel number, will be easier to parse in PD
      int starts_with_f = ((message.byte0 & 0xF0)==0xF0);
      if(!starts_with_f) {
         message.byte0 = (message.byte0 & 0xF0);
         SETFLOAT(&list[0], (float)(message.byte0)); 
      }
      outlet_list(this->outlet_left, gensym("list"), 3, &list[0]);
   }

}

/////////////////////////////////////////////////////////////////////////
// Received float on left inlet. Change midi channel.
//

void midi_in_float(void* x, t_floatarg f) {
  t_midi_in* this = (t_midi_in*)x;

  if((unsigned char)f == this->channel) return;
  
  // Is channel number in range?
  if(f<0 || f>=16 || (float)((int)f)!=(f)){
    error("beaglebone/midi_in: %f is not a valid MIDI channel.", f); 
    this->channel = 0;
    return;
  }

  // Try to set the channel
  int err = beaglebone_pruio_set_midi_channel((int)f);
  if(err){
    error("beaglebone/midi_in: Could not init midi channel (%f), is it already in use?", f);
    this->channel = 0;
    return;
  }
  
  // Store the current channel
  this->channel = (unsigned char)f;
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void *midi_in_new(t_floatarg f) {
   // Create pd object instance
   t_midi_in *x = (t_midi_in *)pd_new(midi_in_class);
   x->outlet_left = outlet_new(&x->x_obj, &s_list);
   x->outlet_right = outlet_new(&x->x_obj, &s_list);

   // Set the channel 
   midi_in_float(x, f);

   // Register the callback.
   if(beaglebone_register_midi_callback(x, midi_in_callback)){
      error("beaglebone/midi_in: Could not init midi_in (%f), is it already in use?", f);
      return NULL;
   }

   return (void *)x;
}

static void midi_in_free(t_midi_in *x) { 
   beaglebone_unregister_callback(BB_CALLBACK_MIDI, 0);
   (void)x;
}

/////////////////////////////////////////////////////////////////////////
// Class definition
// 

void midi_in_setup(void) {
   midi_in_class = class_new(
      gensym("midi_in"), 
      (t_newmethod)midi_in_new, 
      (t_method)midi_in_free, 
      sizeof(t_midi_in), 
      CLASS_DEFAULT,
      A_DEFFLOAT,
      (t_atomtype)0
   );

   class_addfloat(midi_in_class, midi_in_float);
}
