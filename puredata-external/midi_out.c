/* Beaglebone Pru IO 
 * 
 * Copyright (C) 2015 Rafael Vega <rvega@elsoftwarehamuerto.org> 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <m_pd.h>
/* #include <stdio.h> */
/* #include <string.h> */
/* #include <beaglebone_pruio_pins.h> */

#ifdef IS_BEAGLEBONE
#include <beaglebone_pruio.h>
#endif 

#include "beaglebone.h"

/////////////////////////////////////////////////////////////////////////
// Data
//

typedef struct midi_out {
   t_object x_obj;
   t_outlet *outlet_left;
   t_outlet *outlet_right;
} t_midi_out;

// A pointer to the class object.
t_class *midi_out_class;

/////////////////////////////////////////////////////////////////////////
// Received a list of bytes on the left inlet. Send bytes to midi port
//

void midi_out_bytes(t_midi_out *x, t_symbol *s, int argc, t_atom *argv) {
   (void)x;
   (void)s;

   int i;
   for(i=0; i<argc; ++i) {
      unsigned char b = (unsigned int)atom_getint(argv+i);
      beaglebone_pruio_write_midi_message(b);
   }
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void *midi_out_new() {
   // Create pd object instance
   t_midi_out *x = (t_midi_out *)pd_new(midi_out_class);
   x->outlet_left = outlet_new(&x->x_obj, &s_list);
   x->outlet_right = outlet_new(&x->x_obj, &s_list);

   return (void *)x;
}

static void midi_out_free(t_midi_out *x) { 
   beaglebone_unregister_callback(BB_CALLBACK_MIDI, 0);
   (void)x;
}

/////////////////////////////////////////////////////////////////////////
// Class definition
// 

void midi_out_setup(void) {
   midi_out_class = class_new(
      gensym("midi_out"), 
      (t_newmethod)midi_out_new, 
      (t_method)midi_out_free, 
      sizeof(t_midi_out), 
      CLASS_DEFAULT,
      (t_atomtype)0
   );

   class_addanything(midi_out_class, (t_method)midi_out_bytes);
}
